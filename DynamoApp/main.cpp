#include "../DynamoCore/Simulation.h"

int main(int argc, char *argv[])
{
	// First get command line args
	std::string configPath;
	if (argc == 1)
	{
		configPath = "C:\\Projects\\Dynamo\\Parameters\\params.dat";
	}
	else if (argc == 2)
	{
		configPath = argv[1];
	}
	CLogger logger;
	logger.Log("Parameter File Path: " + configPath);
   CSimulation sim;
   if(!sim.Init(configPath))
   {
      return 1;
   }
   sim.Run();
   return 0;
}