########
# Plots boundaries to file
########

set parametric
set angle degree
set trange [0:360]
rinner = 0.6
rtach = 0.7
router = 1.0

set table "bdy_contours.dat"
plot rinner*cos(t), rinner*sin(t), router*cos(t), router*sin(t)
unset table

set table "tach_contours.dat"
plot rtach*cos(t), rtach*sin(t) lc 'black'
unset table
reset

########

set size ratio 1.75
set contour base
unset surface
set view map
unset xtics
unset colorbox
set xrange [0:1]
set yrange [-1:1]

set parametric
set angle degree
set trange [0:360]
rinner = 0.6
router = 1.0

reset
# Set for png output
#set terminal png size 1024,720
#set output 'fields.png'
# Set terminal for output interactive window
set terminal wxt 1

#set contours base 
#unset surface
#set table 'cont.dat'
#splot 'br0.txt' using 1:2:3 with line
#unset table

#reset
#set xrange [0:1]
#set yrange [-1:1]
#unset key
#set palette rgbformulae 33,13,10
#plot 'surf.dat' with image, 'cont.dat' w l lt -1 lw 1.5

# Calculate some values for margins
#if (!exists("MP_LEFT"))   MP_LEFT = .1
#if (!exists("MP_RIGHT"))  MP_RIGHT = .95
#if (!exists("MP_BOTTOM")) MP_BOTTOM = .1
#if (!exists("MP_TOP"))    MP_TOP = .9
#if (!exists("MP_GAP"))    MP_GAP = 0.1

# Initialise multiplot
#set multiplot layout 2,3 rowsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_GAP

# Define some macros to make plotting easier
# "set contours base" - Plots the base contours as well as a surface
# "unset surface" - disables plotting of the surface
# "set view 0,0,1" - sets the view to look top down (down the z axis)
# "unset key" - removes the legend
# angle, trange, rinner and router are all for the boundary plots
#SETUP_POLAR_PLOT = "set xrange [0:1]; \
#set yrange [-1:1]; \
#set contours base; \
#unset surface; \
#set view 0,0,1; \
#set xtics font ', 6' scale 0.25; \
#unset ytics; \
#unset ztics; \
#unset key; \
#set parametric; \
#set cntrparam levels 10; \
#set angle degree; \
#set trange [0:360]; \
#rinner = 0.6; \
#router = 1.0"

#PLOT_BNDY = "replot rinner*cos(t), rinner*sin(t) lc 'black'" #, router*cos(t), router*sin(t) lc 'black'"

#set label 1
#@SETUP_POLAR_PLOT
#set title "Radial Field"
#splot 'br0.txt' using 1:2:3 with line, rinner*cos(t), rinner*sin(t) lc 'black'
#@PLOT_BNDY

#set label 2
#@SETUP_POLAR_PLOT
#set title "Latitudinal Field"
#splot 'bth0.txt' using 1:2:3 with line

#set label 3
#@SETUP_POLAR_PLOT
#set title "Azimuthal Field"
#splot 'bphi0.txt' using 1:2:3 with line

#set label 4
#@SETUP_POLAR_PLOT
#set title "Poloidal Field"
#splot 'a0.txt' using 1:2:3 with line

#set label 5
#@SETUP_POLAR_PLOT
#set title "Toroidal Field"
#splot 'b0.txt' using 1:2:3 with line

#unset multiplot

reset 
set terminal pngcairo size 800,800 
set output '3d-polar.png'

set lmargin at screen 0.05
set rmargin at screen 0.85
set bmargin at screen 0.1
set tmargin at screen 0.9

set pm3d map interpolate 20,20
unset key
set multiplot

# plot the heatmap
set cntrparam bspline
set cntrparam points 10
set cntrparam levels increment -6,-6,-24
set contour surface
#set style increment user           #NOTE: the commented out lines do not seem to affect color or width of the the contour lines no matter what number I use for the linetype
#set linetype 8 lc rgb "blue" lw 2
#set linetype 9 lc rgb "black" lw 1
#set linetype 10 lc rgb "orange" lw 1
#set linetype 11 lc rgb "yellow" lw 1

set palette rgb 33,13,10 #rainbow (blue-green-yellow-red)
set cbrange [-18:0]

unset border
unset xtics
unset ytics

set xrange [0:1]
set yrange [-1:1]
set colorbox user origin 0.9,0.1 size 0.03,0.8
splot 'a0.txt'

# now plot the polar grid only
set style line 11 lc rgb 'black' lw 2 lt 0
set grid polar ls 11
set polar
set logscale r 10
set rrange[10:20000]
unset raxis
set rtics format '' scale 0
#set rtics axis scale 
set rtics (20,50,100,200,500,1000,2000,5000,10000,20000)
do for [i=-150:180:30] {
dum = r+0.15+0.05*int(abs(i/100))+0.05*int(abs(i/140))-0.05/abs(i+1)
set label i/30+6 at first dum*cos(i), first dum*sin(i) center sprintf('%d', i)
}
set label 20 at first 0, first -(log(20)/log(10)-1) center "20"
set label 100 at first 0, first -(log(100)/log(10)-1) center "100"
set label 200 at first 0, first -(log(200)/log(10)-1) center "200"
set label 1000 at first 0, first -(log(1000)/log(10)-1) center "1k"
set label 2000 at first 0, first -(log(2000)/log(10)-1) center "2k"
set label 10000 at first 0, first -(log(10000)/log(10)-1) center "10k"
set label 20000 at first 0, first -(log(20000)/log(10)-1) center "20k"
plot NaN w l
unset multiplot
unset output



splot 'a0.txt' using 1:2:3 with line

pause -1