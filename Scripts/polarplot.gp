reset 

# run with gnuplot -e "filepath='C:\\Projects\\simulation\\araddifftmp1.txt'" ../../polarplot.gp

if (!exists("filepath")) filepath='C:\\Projects\\simulation\\araddifftmp1.txt'

set termopt enhanced

set size ratio 1.75
set xrange [0:1]
set yrange [-1:1]
set isosamples 50
set pm3d
unset surface
unset key
set view map
unset colorbox
unset xtics
set palette defined ( 0 "green", 1 "blue", 2 "orange", 3 "red" ) 
splot filepath using 1:2:3

pause -1