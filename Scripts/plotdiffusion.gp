set terminal png

set output 'diffusion.png'
set xrange [0.6:1.0]
set title "Diffusivity Profile"
plot "eta.txt" using 1:2 with lines

set output 'diffusion_derivative.png'
set xrange [0.6:1.0]
set title "Radial Derivative of Diffusivity Profile"
plot "deta.txt" using 1:2 with lines

set term wxt 0
set xrange [0.6:1.0]
set title "Diffusivity Profile"
plot "eta.txt" using 1:2 with lines

set term wxt 1
set xrange [0.6:1.0]
set title "Radial Derivative of Diffusivity Profile"
plot "deta.txt" using 1:2 with lines

pause -1