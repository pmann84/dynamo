import os
import re
from matplotlib.pyplot import *


def get_available_indices_for_filename(datadir, name):
    available_indices = []
    for datafile in os.listdir(datadir):
        regex_str = "{}([0-9]+)\.txt".format(name)
        regex = re.match(regex_str, datafile)
        if regex:
            available_indices.append(int(regex.groups()[0]))
    return available_indices


def get_range_for_filename(datadir, name):
    available_indices = get_available_indices_for_filename(datadir, name)
    return min(available_indices), max(available_indices)


def validate_index_for_file(datadir, plot_title, index):
    index_valid = True
    valid_range = get_range_for_filename(datadir, plot_title.lower())
    if index not in range(valid_range[0], valid_range[1] + 1):
        index_valid = False
    if valid_range[0] == valid_range[1] == index:
        index_valid = True
    return index_valid


def setup_plot_for_butterfly(ax, plot_title, t, theta, values):
    # Set the title
    ax.set_title(plot_title)
    # Set tick values
    # ax.set_xticks(np.pi / 180. * np.linspace(0, 180, 2, endpoint=False))  # theta ticks
    # ax.set_xticklabels([])
    # ax.set_rticks([0.0, 0.6, 0.7, 1.0])
    ax.set_xlabel("Time")
    ax.set_ylabel("Latitude")
    ax.set_yticks([-90.0, -45.0, 0.0, 45.0, 90.0])
    # ax.set_yticklabels([])
    # Set contour colour scheme
    magma()
    # Plot shit!
    ax.contourf(t, theta, values, 20)


def setup_plot_for_1d(ax, plot_title, x, y, x_title, y_title):
    # Set the title
    ax.set_title(plot_title)
    # Set tick values
    # ax.set_xticks(np.pi / 180. * np.linspace(0, 180, 2, endpoint=False))  # theta ticks
    # ax.set_xticklabels([])
    # ax.set_rticks([0.0, 0.6, 0.7, 1.0])
    # ax.set_yticklabels([])
    ax.set_xlabel(x_title)
    ax.set_ylabel(y_title)
    ax.plot(x, y)


def setup_plot_for_polar_field(ax, plot_title, r, theta, values):
    # Set the title
    ax.set_title(plot_title)
    # Reorient 0 to the north
    ax.set_theta_zero_location("N")
    ax.set_theta_direction(-1)
    # Set tick values
    ax.set_xticks(np.pi / 180. * np.linspace(0, 180, 2, endpoint=False))  # theta ticks
    ax.set_xticklabels([])
    ax.set_rticks([0.0, 0.6, 0.7, 1.0])
    ax.set_yticklabels([])
    # Set contour colour scheme
    magma()
    # Plot shit!
    ax.contourf(theta, r, values, 20)
    # Set min and max values
    # IMPORTANT that these are done AFTER we actually plot the data
    # otherwise they get overwritten
    ax.set_thetamin(0.0)
    ax.set_thetamax(180.0)
    ax.set_rmin(0.0)
    ax.set_rmax(1.0)


def read_time_data(datadir):
    time_data = {"nt": 0,
                 "dt": 0.0,
                 "butterfly": {"start": 0,
                               "end": 0,
                               "interval": 0},
                 "profiles": {"start": 0,
                              "end": 0,
                              "interval": 0},
                 "quantities": {"start": 0,
                                "end": 0,
                                "interval": 0}}

    full_path = os.path.join(datadir, "time.txt")
    if os.path.exists(full_path):
        with open(full_path, "r") as f:
            data = f.readlines()
            time_data["nt"] = int(data[0])
            time_data["dt"] = float(data[1])
            time_data["butterfly"]["start"] = int(data[2])
            time_data["butterfly"]["end"] = int(data[3])
            time_data["butterfly"]["interval"] = int(data[4])
            time_data["profiles"]["start"] = int(data[5])
            time_data["profiles"]["end"] = int(data[6])
            time_data["profiles"]["interval"] = int(data[7])
            time_data["quantities"]["start"] = int(data[8])
            time_data["quantities"]["end"] = int(data[9])
            time_data["quantities"]["interval"] = int(data[10])
        return time_data
    else:
        return None


def read_1d_data_from_file(filename, start=-1, end=-1, time_dict=None):
    # Read the data from the file
    final_data = []
    if os.path.exists(filename):
        with open(filename, "r") as f:
            data = f.readlines()
            for d in data:
                final_data.append(float(d))
    else:
        raise "Path to {} does not exist!".format(filename)

    # Truncate time slices depending on indexes
    if start != -1 and end != -1:
        final_data = final_data[int(start):int(end)-1]

    num_data_points = len(final_data)

    # Generate temporal mesh
    t_mesh_data = generate_time_mesh(num_data_points, time_dict, start, end)

    # print("Time mesh size: {}".format(t_mesh_data.size))
    # print("Data size: {}".format(len(final_data)))
    return t_mesh_data, final_data


def read_1d_data_with_axis_from_file(filename):
    # Read the data from the file
    axis_data = []
    final_data = []
    if os.path.exists(filename):
        with open(filename, "r") as f:
            data = f.readlines()
            for d in data:
                r, val = d.split()
                axis_data.append(float(r))
                final_data.append(float(val))
    else:
        raise "Path to {} does not exist!".format(filename)

    num_data_points = len(final_data)
    # print("Time mesh size: {}".format(t_mesh_data.size))
    # print("Data size: {}".format(len(final_data)))
    return axis_data, final_data


def read_butterfly_data_from_file(filename, start=-1, end=-1, time_dict=None):
    # Read the data from the file
    field_data = []

    if os.path.exists(filename):
        with open(filename, "r") as f:
            data = f.readlines()
            for d in data:
                time_slice_array = [float(x) if x != "-nan(ind)" else 0 for x in d.split()]
                field_data.append(time_slice_array)
    else:
        raise "Path to {} does not exist!".format(filename)

    # Truncate time slices depending on indexes
    if start != -1 and end != -1:
        field_data = field_data[int(start):int(end)]

    num_data_points = len(field_data)

    # Generate temporal mesh
    t_mesh_data = generate_time_mesh(num_data_points, time_dict, start, end)

    # Generate theta mesh
    th_mesh_data = np.linspace(-90.0, 90.0, len(field_data[0]))

    return t_mesh_data, th_mesh_data, field_data


def read_2d_field_data_from_file(filename):
    # Read the data from the file
    field_data = []
    r_mesh_data = []
    th_mesh_data = []
    tmp_row = []
    th_mesh_recorded = False
    tmp_counter = 0
    if os.path.exists(filename):
        with open(filename, "r") as f:
            data = f.readlines()
            for d in data:
                tmp_counter += 1
                split_d = d.split()
                if len(split_d) > 0:
                    if not th_mesh_recorded:
                        th_mesh_data.append(float(split_d[1]))
                    if tmp_counter == 1:
                        r_mesh_data.append(float(split_d[0]))
                    data_val = split_d[2]
                    if data_val == "-nan(ind)":
                        tmp_row.append(0.0)
                    else:
                        tmp_row.append(float(split_d[2]))
                else:
                    tmp_counter = 0
                    th_mesh_recorded = True
                    field_data.append(tmp_row[:])
                    tmp_row.clear()
        return r_mesh_data, th_mesh_data, field_data
    else:
        raise "Path to {} does not exist!".format(filename)


def convert_2d_field_data_to_graph_format(r_mesh, th_mesh, data):
    r_array = np.asarray(r_mesh)
    th_array = np.asarray(th_mesh)

    # -- Generate Data -----------------------------------------
    # Get the mesh grid
    theta, r = np.meshgrid(th_array, r_array)
    # Convert the values to the correct size
    data_array = np.asarray(data)
    values = np.reshape(data_array, (r_array.size, th_array.size))
    return r, theta, values


def convert_butterfly_data_to_graph_format(t_mesh, th_mesh, data):
    t_array = np.asarray(t_mesh)
    th_array = np.asarray(th_mesh)
    # -- Generate Data -----------------------------------------
    # Get the mesh grid
    t, theta = np.meshgrid(t_array, th_array)
    # Convert the values to the correct size
    data_array = np.asarray(data)
    # print(len(data_array), t_array.size, th_array.size)
    values = np.reshape(data_array, (t_array.size, th_array.size)).transpose()
    return t, theta, values


def generate_time_mesh(num_data_points, time_dict, start=-1, end=-1):
    # Initialise dt in the absence of timing data its 1 timestep
    dt = 1
    # Get the start step - if it specified use that, otherwise set to 0
    start_step = 0 if start == -1 else start
    # Get the end step - if it specified use that, otherwise set to number of points loaded
    end_step = num_data_points if end == -1 else end
    # print("Range calculated after data read: {}-{}".format(start_step, end_step))
    t_mesh_data = []
    # If we have time data then generate meshes
    if time_dict:
        # Calculate the actual time step which is interval * dt
        dt = time_dict["dt"]
        actual_start_step = time_dict["quantities"]["start"] + start_step * time_dict["quantities"]["interval"]
        actual_end_step = time_dict["quantities"]["start"] + end_step * time_dict["quantities"]["interval"]
        # print("Range calculated using time info: {}-{}".format(actual_start_step, actual_end_step))
        start_time = float(actual_start_step) * dt
        end_time = float(actual_end_step) * dt
        # print("Time range: {}-{}".format(start_time, end_time))
        # VALIDATE THE START/END STEPS BASED ON THE ACTUAL START/END
        # Generate the mesh
        t_mesh_data = np.linspace(start_time, end_time, num_data_points)
    # Just generate integer shizz
    else:
        t_mesh_data = np.linspace(int(start_step), int(end_step), num_data_points)
    return t_mesh_data
