import os
import argparse
from matplotlib.pyplot import *

import plot_lib


def process_args():
    # Main parser with data dir argument
    parser = argparse.ArgumentParser(prog='master_plot')
    parser.add_argument('--datadir', type=str)

    # Sub parser object - used to create sub commands
    subparsers = parser.add_subparsers()

    # Parser for the profiles plotting arguments
    field_parser = subparsers.add_parser('fields', help='Plot field profiles for output time steps.')
    field_parser.add_argument('index', type=int, help='Index of the time-step to be plotted.')
    field_parser.add_argument('--fields', nargs='+', type=str, choices=['a', 'b'], help='List of the fields to plot, if not specifed all available fields will be plotted.')
    field_parser.set_defaults(func=field_plot)

    # Parser for energy plotting
    energy_parser = subparsers.add_parser('energy', help='Plot energy graphs.')
    energy_parser.add_argument("--start", "-s")
    energy_parser.add_argument("--end", "-e")
    energy_parser.set_defaults(func=energy_plot)

    # Parser for butterfly plotting
    butterfly_parser = subparsers.add_parser('butterfly', help='Plot butterfly diagrams for toroidal and radial fields.')
    butterfly_parser.add_argument("--start", "-s")
    butterfly_parser.add_argument("--end", "-e")
    butterfly_parser.set_defaults(func=butterfly_plot)

    # Parser for profile plotting
    profile_parser = subparsers.add_parser('profile', help='Plot static profile plots.')
    profile_subparsers = profile_parser.add_subparsers()
    # Diffusivity parser
    diff_parser = profile_subparsers.add_parser('diff', help='Plot diffusivity profiles.')
    diff_parser.set_defaults(func=diff_plot)
    # Rotation parser
    rot_parser = profile_subparsers.add_parser('rot', help='Plot Differential Rotation profiles.')
    rot_parser.set_defaults(func=rot_plot)
    # Circulation parser
    circ_parser = profile_subparsers.add_parser('circ', help='Plot Meridional Circulation profiles.')
    circ_parser.set_defaults(func=circ_plot)
    # Source parser
    src_parser = profile_subparsers.add_parser('src', help='Plot Source term profiles.')
    src_parser.set_defaults(func=src_plot)

    args = parser.parse_args()
    func_op = getattr(args, 'func', None)
    if func_op is None:
        parser.print_help()
    else:
        args.func(args)


def get_data_dir(args):
    return os.getcwd() if not args.datadir else args.datadir


def diff_plot(args):
    print("Plotting profiles...")
    data_dir = get_data_dir(args)
    print("Data directory: {}".format(data_dir))

    # Get the files
    path_map = {
        "eta": [os.path.join(data_dir, "eta.txt"), "Magnetic Diffusivity", 0],
        "deta": [os.path.join(data_dir, "deta.txt"), "Magnetic Diffusivity Gradient", 1],
    }

    # Generate figure
    fig, axarr = subplots(2, 1)
    for plot_key, plot_info in path_map.items():
        # Get the plotting info
        plot_path = plot_info[0]
        plot_title = plot_info[1]
        axis_col = plot_info[2]
        # Read the data from the file
        r, data = plot_lib.read_1d_data_with_axis_from_file(plot_path)
        # Generate the plot
        plot_lib.setup_plot_for_1d(axarr[axis_col], plot_title, r, data, 'r', plot_key)
    # Show it!
    show()


def rot_plot(args):
    print("Plotting profiles...")
    data_dir = get_data_dir(args)
    print("Data directory: {}".format(data_dir))

    # Get the files
    path_map = {
        "omega": [os.path.join(data_dir, "diffrot.txt"), "Differential Rotation", 0],
        "omega_r": [os.path.join(data_dir, "radshear.txt"), "Radial Shear", 1],
        "omega_th": [os.path.join(data_dir, "latshear.txt"), "Latitudinal Shear", 2]
    }

    # Generate figure
    plot_field_from_map(path_map)


def circ_plot(args):
    print("Plotting profiles...")
    data_dir = get_data_dir(args)
    print("Data directory: {}".format(data_dir))

    # Get the files
    path_map = {
        "ur": [os.path.join(data_dir, "ur.txt"), "Radial Velocity", 0],
        "uth": [os.path.join(data_dir, "uth.txt"), "Latitudinal Velocity", 1]
    }

    # Generate figure
    plot_field_from_map(path_map)


def src_plot(args):
    print("Plotting profiles...")
    data_dir = get_data_dir(args)
    print("Data directory: {}".format(data_dir))

    # Get the files
    path_map = {
        "alpha": [os.path.join(data_dir, "alpha.txt"), "Classical Alpha Effect", 0],
        "alpha_bl": [os.path.join(data_dir, "alpha_bl.txt"), "Babcock-Leighton Source", 1]
    }

    # Generate figure
    plot_field_from_map(path_map)


def plot_field_from_map(path_map):
    # Generate figure
    fig, axarr = subplots(1, len(path_map), subplot_kw=dict(projection='polar'))
    for plot_key, plot_info in path_map.items():
        # Get the plotting info
        plot_path = plot_info[0]
        plot_title = plot_info[1]
        axis_col = plot_info[2]

        # Read the data from the file
        r_mesh, th_mesh, data = plot_lib.read_2d_field_data_from_file(plot_path)
        r, theta, values = plot_lib.convert_2d_field_data_to_graph_format(r_mesh, th_mesh, data)

        # Generate the plot
        plot_lib.setup_plot_for_polar_field(axarr[axis_col], plot_title, r, theta, values)
    # Show it!
    show()


def butterfly_plot(args):
    print("Plotting energy...")
    data_dir = get_data_dir(args)
    print("Data directory: {}".format(data_dir))

    # Get the files
    path_map = {
        "BPhi": [os.path.join(data_dir, "butt.txt"), 0],
        "Br": [os.path.join(data_dir, "radbutt.txt"), 1]
    }

    # Get start and end if given
    start = -1 if not args.start else int(args.start)
    end = -1 if not args.end else int(args.end)

    # Load time info
    time_dict = plot_lib.read_time_data(data_dir)

    # Generate figure
    fig, axarr = subplots(2, 1)
    for plot_title, plot_info in path_map.items():
        # Get the plotting info
        plot_path = plot_info[0]
        axis_col = plot_info[1]
        # Read the data from the file
        t_mesh, th_mesh, data = plot_lib.read_butterfly_data_from_file(plot_path, start, end, time_dict)
        t, theta, values = plot_lib.convert_butterfly_data_to_graph_format(t_mesh, th_mesh, data)
        # Generate the plot
        plot_lib.setup_plot_for_butterfly(axarr[axis_col], plot_title, t, theta, values)
    # Show it!
    show()


def energy_plot(args):
    print("Plotting energy...")
    data_dir = get_data_dir(args)
    print("Data directory: {}".format(data_dir))

    # Get the files
    full_path = os.path.join(data_dir, "energy.txt")

    # Get start and end if given
    start = -1 if not args.start else int(args.start)
    end = -1 if not args.end else int(args.end)

    # Load time info
    time_dict = plot_lib.read_time_data(data_dir)

    print("Plotting range: {}-{}".format(start, end))

    # Generate figure
    fig, ax = subplots()
    t_mesh, data = plot_lib.read_1d_data_from_file(full_path, start, end, time_dict)

    # Calculate energy spectra
    energy_spectra = np.fft.fft(data)
    step_dt = time_dict['dt']*time_dict['quantities']['interval']
    freq = np.fft.fftfreq(len(data), step_dt)

    # Generate the plot
    plot_lib.setup_plot_for_1d(ax, "Em", t_mesh, data, "Time", "Mag. Energy")
    # Show it!
    show()


def field_plot(args):
    print("Plotting profiles...")
    data_dir = get_data_dir(args)
    print("Data directory: {}".format(data_dir))

    # Get the files
    path_map = {
        "a": [os.path.join(data_dir, "a{}.txt".format(args.index)), 0],
        "b": [os.path.join(data_dir, "b{}.txt".format(args.index)), 1],
        # "BR": [os.path.join(data_dir, "br{}.txt".format(args.index)), 0, 0],
        # "BTH": [os.path.join(data_dir, "bth{}.txt".format(args.index)), 0, 1],
        # "BPHI": [os.path.join(data_dir, "bphi{}.txt".format(args.index)), 0, 2]
    }

    # Validate the index passed in
    index_valid = True
    for plot_title, plot_info in path_map.items():
        if not plot_lib.validate_index_for_file(data_dir, plot_title, args.index):
            index_valid = False
    if not index_valid:
        print("ERROR: Index {} does not exist for all profiles!".format(args.index))
        sys.exit()

    # Get the actual list of things to plot
    temp = args.fields if args.fields else path_map.keys()
    fields_to_plot = [f.lower() for f in temp]

    # Generate figure
    fig, axarr = subplots(1, len(fields_to_plot), subplot_kw=dict(projection='polar'))
    for field in fields_to_plot:
        # Get the plotting info
        plot_path = path_map[field][0]
        axis_col = path_map[field][1]

        # Read the data from the file
        r_mesh, th_mesh, data = plot_lib.read_2d_field_data_from_file(plot_path)
        r, theta, values = plot_lib.convert_2d_field_data_to_graph_format(r_mesh, th_mesh, data)

        # Generate the plot
        axis = axarr if isinstance(axarr, matplotlib.axes.Axes) else axarr[axis_col]
        plot_lib.setup_plot_for_polar_field(axis, field, r, theta, values)
    # Show it!
    show()


if __name__ == "__main__":
    process_args()

