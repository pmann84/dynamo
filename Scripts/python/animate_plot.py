import os
import sys
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import argparse

import plot_lib


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("name")
    parser.add_argument("--start", "-s")
    parser.add_argument("--end", "-e")
    parser.add_argument("--datadir")
    args = parser.parse_args()
    return args


def generate_plot(plot_ax, plot_title):
    # Reorient 0 to the north
    plot_ax.set_theta_zero_location("N")
    plot_ax.set_theta_direction(-1)
    # Set tick values
    plot_ax.set_xticks(np.pi / 180. * np.linspace(0, 180, 2, endpoint=False))  # theta ticks
    plot_ax.set_xticklabels([])
    plot_ax.set_rticks([0.0, 0.6, 0.7, 1.0])
    plot_ax.set_yticklabels([])
    # Set min and max values
    # IMPORTANT that these are done AFTER we actually plot the data
    # otherwise they get overwritten
    plot_ax.set_thetamin(0.0)
    plot_ax.set_thetamax(180.0)
    plot_ax.set_rmin(0.0)
    plot_ax.set_rmax(1.0)
    # Set contour colour scheme
    plt.magma()

anim_data = []
# Start the figure
fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))
generate_plot(ax, "")
# x, y here (0,0) is lower left and (1,1) is upper right
time_text = ax.text(0.5, 0.05, '', horizontalalignment='left', verticalalignment='top', transform=ax.transAxes)
frame_text = ax.text(0.5, 0.01, '', horizontalalignment='left', verticalalignment='top', transform=ax.transAxes)
g_time_dict = {}
g_start_idx = 0
g_end_idx = 0


def read_data_from_file(filename):
    # Read the data from the file
    field_data = []
    r_mesh_data = []
    th_mesh_data = []
    tmp_row = []
    th_mesh_recorded = False
    tmp_counter = 0
    if os.path.exists(filename):
        with open(filename, "r") as f:
            data = f.readlines()
            for d in data:
                tmp_counter += 1
                split_d = d.split()
                if len(split_d) > 0:
                    if not th_mesh_recorded:
                        th_mesh_data.append(float(split_d[1]))
                    if tmp_counter == 1:
                        r_mesh_data.append(float(split_d[0]))
                    data_val = split_d[2]
                    if data_val == "-nan(ind)":
                        tmp_row.append(0.0)
                    else:
                        tmp_row.append(float(split_d[2]))
                else:
                    tmp_counter = 0
                    th_mesh_recorded = True
                    field_data.append(tmp_row[:])
                    tmp_row.clear()
        return r_mesh_data, th_mesh_data, field_data
    else:
        raise "Path to {} does not exist!".format(filename)


def convert_data_to_graph_format(r_mesh, th_mesh, data):
    r_array = np.asarray(r_mesh)
    th_array = np.asarray(th_mesh)

    # -- Generate Data -----------------------------------------
    # Get the mesh grid
    theta, r = np.meshgrid(th_array, r_array)
    # Convert the values to the correct size
    data_array = np.asarray(data)
    values = np.reshape(data_array, (r_array.size, th_array.size))
    return r, theta, values


def animate(i):
    # Plot shit!
    if i < len(anim_data):
        data_dict = anim_data[i]
        theta = data_dict["th"]
        r = data_dict["r"]
        values = data_dict["data"]
        con = ax.contourf(theta, r, values, 20)
        # Set min and max values
        # IMPORTANT that these are done AFTER we actually plot the data
        # otherwise they get overwritten
        ax.set_thetamin(0.0)
        ax.set_thetamax(180.0)
        ax.set_rmin(0.0)
        ax.set_rmax(1.0)
        # Get the frame
        frame = g_start_idx*g_time_dict["profiles"]["interval"]+i
        frame_time = frame*g_time_dict["dt"]
        time_text.set_text('t = %.6f' % frame_time)
        frame_text.set_text('timestep = %0.1d' % frame)
        return [con, time_text, frame_text]


def main(args):
    datadir = args.datadir
    if not args.datadir:
        # Assume its the current directory
        datadir = os.getcwd()

    ax.set_title(args.name)

    # Determine range in file
    start_idx, end_idx = plot_lib.get_range_for_filename(datadir, args.name)
    print("Range available: {}-{}".format(start_idx, end_idx))

    # Check for overridden arguments
    if args.start and args.end:
        if int(args.start) > int(args.end):
            print("ERROR: Given indices are ill-formed. Start is greater than the end!")
            sys.exit(1)

    if args.start:
        if int(args.start) < start_idx or int(args.start) > end_idx:
            print("ERROR: Given index [{}] is out of valid range {}".format(args.start, [start_idx, end_idx]))
            sys.exit(1)
        start_idx = int(args.start)
    if args.end:
        if int(args.end) < start_idx or int(args.end) > end_idx:
            print("ERROR: Given index [{}] is out of valid range {}".format(args.end, [start_idx, end_idx]))
            sys.exit(1)
        end_idx = int(args.end)

    # Get time info
    global g_time_dict, g_start_idx, g_end_idx
    g_time_dict = plot_lib.read_time_data(datadir)
    g_start_idx = start_idx
    g_end_idx = end_idx

    # Load the data
    for idx in range(start_idx, end_idx+1):
        filepath = os.path.join(datadir, "{}{}.txt".format(args.name, idx))
        r_mesh, th_mesh, data = read_data_from_file(filepath)
        r, theta, values = convert_data_to_graph_format(r_mesh, th_mesh, data)
        anim_data.append({"index": idx, "data": values, "r": r, "th": theta})
    print("{} frames loaded!".format(len(anim_data)))

    anim = animation.FuncAnimation(fig, animate, frames=len(anim_data)+1)
    #FFwriter = animation.FFMpegWriter()
    #anim.save('{}.mp4'.format(args.name), writer=FFwriter) #, fps=30, extra_args=['-vcodec', 'libx264'])
    plt.show()

if __name__ == "__main__":
    main(parse_args())
