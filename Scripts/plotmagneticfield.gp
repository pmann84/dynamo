reset 

# run with gnuplot -e "idx='1'" ../../Scripts/plotmagneticfield.gp

if (!exists("idx")) idx=0

set termopt enhanced

# Calculate some values for margins
if (!exists("MP_LEFT"))   MP_LEFT = .1
if (!exists("MP_RIGHT"))  MP_RIGHT = .95
if (!exists("MP_BOTTOM")) MP_BOTTOM = .1
if (!exists("MP_TOP"))    MP_TOP = .9
if (!exists("MP_GAP"))    MP_GAP = 0.1

# Initialise multiplot
set multiplot layout 2,3 rowsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_GAP

# . is string concatenation
BR_FILE='br'.idx.'.txt'
BTH_FILE='bth'.idx.'.txt'
BPHI_FILE='bphi'.idx.'.txt'
A_FILE='a'.idx.'.txt'
B_FILE='b'.idx.'.txt'

set size ratio 1.75
set xrange [0:1]
set yrange [-1:1]
set isosamples 500
set pm3d
unset surface
unset key
set view map
unset colorbox
set ytics 0.1
set ytics font ",6" 
unset xtics
set palette defined ( 0 "green", 1 "blue", 2 "orange", 3 "red" ) 

set title "B_{r}"
splot BR_FILE using 1:2:3

set title "B_{theta}"
splot BTH_FILE using 1:2:3

set title "B_{phi}"
splot BPHI_FILE using 1:2:3

set title "A"
splot A_FILE using 1:2:3

set title "B"
splot B_FILE using 1:2:3

unset multiplot

pause -1