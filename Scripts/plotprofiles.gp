reset 

# run with gnuplot ../../Scripts/plotprofiles.gp

set termopt enhanced

# Calculate some values for margins
if (!exists("MP_LEFT"))   MP_LEFT = .1
if (!exists("MP_RIGHT"))  MP_RIGHT = .95
if (!exists("MP_BOTTOM")) MP_BOTTOM = .1
if (!exists("MP_TOP"))    MP_TOP = .9
if (!exists("MP_GAP"))    MP_GAP = 0.1

# Initialise multiplot
set multiplot layout 3,3 rowsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_GAP

# . is string concatenation
DIFFROT_FILE='diffrot.txt'
LATSHEAR_FILE='latshear.txt'
RADSHEAR_FILE='radshear.txt'
STREAM_FILE='stream.txt'
UR_FILE='ur.txt'
UTH_FILE='uth.txt'
ALPHA_FILE='alpha.txt'
ETA_FILE='eta.txt'
DETA_FILE='deta.txt'

# Set some global stuff
set palette defined ( 0 "green", 1 "blue", 2 "orange", 3 "red" ) 
set size ratio 1.75
set xrange [0:1]
set yrange [-1:1]
set pm3d
unset surface
set view map
unset key
unset colorbox
unset xtics

# Now do the plots
set title "Differential Rotation"
splot DIFFROT_FILE using 1:2:3

set title "Latitudinal Shear"
splot LATSHEAR_FILE using 1:2:3

set title "Radial Shear"
splot RADSHEAR_FILE using 1:2:3

set title "Stream Function"
splot STREAM_FILE using 1:2:3

set title "Radial Meridional Flow"
splot UR_FILE using 1:2:3

set title "Latitudinal Meridional Flow"
splot UTH_FILE using 1:2:3

set title "Poloidal Source"
splot ALPHA_FILE using 1:2:3

# Set some different ranges for the 
# 2D plots
set xrange [0.6:1.0]
set yrange [0:1.1]
set title "Diffusivity"
plot ETA_FILE using 1:2 with lines

set xrange [0.6:1.0]
set yrange [0:30]
set title "Radial Derivative of Diffusivity"
plot DETA_FILE using 1:2 with lines

unset multiplot

pause -1