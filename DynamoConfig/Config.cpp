#include "Config.h"

CConfig::CConfig(IConfigValidator& validator) : m_validator(validator)
{
}

bool CConfig::Read(std::string filename)
{
	// Check file existence
	if(!std::experimental::filesystem::exists(filename))
	{
		throw XConfigFileDoesNotExist(filename);
	}

	// Clear all existing parameters on Read
	m_params.clear();

	// Read from given file
	m_filename = filename;
	std::ifstream infile;
	std::string line;
	infile.open(filename);

	std::string varname, vartype, varval;

	if (infile.is_open())
	{
		while (getline(infile, line))
		{
			if (!(line.substr(0, 2) == "//" || line == ""))
			{
				std::vector<std::string> split_str;
				StringUtils::split(line, " ", split_str);
				varname = StringUtils::trimWhitespace(split_str[0]);
				varval = StringUtils::trimWhitespace(split_str[1]);
				varval = varval == "PI" ? "M_PI" : varval;
				m_params.push_back(CParameter(varname, varval));
			}
		}
		infile.close();
	}

   return m_validator.validate(*this);
}

std::string CConfig::getConfigFilename() const
{
	return m_filename;
}

CParameter CConfig::getParamByName(const std::string name) const
{
	for (auto p : m_params)
	{
		if (p.Name() == name)
		{
			return p;
		}
	}
	throw XConfigParameterDoesNotExist(name);
}

int CConfig::NumParams() const
{
	return m_params.size();
}

void CConfig::printParams()
{
	for (auto p : m_params)
	{
		std::cout << p.Name() << " = " << p.Value() << std::endl;
	}
}
