﻿#ifndef __PARAMETER_H__
#define __PARAMETER_H__

#define _USE_MATH_DEFINES // Gives access to M_PI in math.h

#include <string>
#include <algorithm>
#include <sstream>
#include <math.h>

class CParameter
{
public:
	explicit CParameter(std::string name);
	CParameter(std::string name, std::string value);

	~CParameter() = default;

	std::string Name() const;
	std::string Value() const;

	operator double() const;
	operator std::string() const;
	operator int() const;
   operator unsigned() const;
	operator bool() const;

private:
	std::string m_name;
	std::string m_value;
};

#endif // __PARAMETER_H__
