#include "StringUtils.h"

unsigned int StringUtils::split(const std::string &txt, std::string split_str, std::vector<std::string> &strs)
{
	size_t pos = txt.find(split_str);
	unsigned int initialPos = 0;
	strs.clear();

	// Decompose statement
	while (pos != std::string::npos) {
		strs.push_back(txt.substr(initialPos, pos - initialPos + 1));
		initialPos = pos + 1;

		pos = txt.find(split_str, initialPos);
	}

	// Add the last one
	strs.push_back(txt.substr(initialPos, std::min(pos, txt.size()) - initialPos + 1));

	return strs.size();
}

void StringUtils::trimWhitespaceLeftInline(std::string &s)
{
   s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int c) {return !std::isspace(c); }));
}

void StringUtils::trimWhitespaceRightInline(std::string &s)
{
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int c) {return !std::isspace(c); }).base(), s.end());
}

void StringUtils::trimWhitespaceInline(std::string &s)
{
	trimWhitespaceLeftInline(s);
	trimWhitespaceRightInline(s);
}

std::string StringUtils::trimWhitespaceLeft(std::string s)
{
	StringUtils::trimWhitespaceLeftInline(s);
	return s;
}

std::string StringUtils::trimWhitespaceRight(std::string s)
{
	StringUtils::trimWhitespaceRightInline(s);
	return s;
}

std::string StringUtils::trimWhitespace(std::string s)
{
	StringUtils::trimWhitespaceInline(s);
	return s;
}