#ifndef __CONFIG_H__
#define __CONFIG_H__

#define _USE_MATH_DEFINES // Gives access to M_PI in math.h

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <experimental/filesystem>

#include "ConfigExceptions.h"
#include "Parameter.h"
#include "StringUtils.h"
#include "IConfigValidator.h"

// TODO: Add ability to read config from a map

class CConfig
{
public:
   CConfig(IConfigValidator& validator);
	~CConfig() = default;

	bool Read(std::string filename);

	std::string getConfigFilename() const;
	CParameter getParamByName(const std::string name) const;
	int NumParams() const; 

	void printParams();

private:
	std::string m_filename;
	std::vector<CParameter> m_params;
   IConfigValidator& m_validator;
};

#endif // __CONFIG_H__