#ifndef __STRING_UTILS_H__
#define __STRING_UTILS_H__

#include <string>
#include <vector>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

namespace StringUtils
{
	unsigned int split(const std::string &txt, std::string split_str, std::vector<std::string> &strs);

	void trimWhitespaceLeftInline(std::string &s);
	void trimWhitespaceRightInline(std::string &s);
	void trimWhitespaceInline(std::string &s);

	std::string trimWhitespaceLeft(std::string s);
	std::string trimWhitespaceRight(std::string s);
	std::string trimWhitespace(std::string s);
}


#endif // __STRING_UTILS_H__