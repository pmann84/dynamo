#ifndef __ICONFIG_VALIDATOR_H__
#define __ICONFIG_VALIDATOR_H__

class CConfig;

class IConfigValidator
{
public:
   virtual ~IConfigValidator() {}
   virtual bool validate(const CConfig&) = 0;
};

#endif // __ICONFIG_VALIDATOR_H__