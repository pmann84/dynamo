#ifndef __CONFIG_EXCEPTIONS_H__
#define __CONFIG_EXCEPTIONS_H__

#include <string>

class XConfigParameterDoesNotExist : public std::runtime_error
{
public:
	explicit XConfigParameterDoesNotExist(): std::runtime_error("Parameter does not exist!")
	{
	}

	explicit XConfigParameterDoesNotExist(const std::string& name) : std::runtime_error("Parameter [" + name + "] does not exist!")
	{
	}
};

class XConfigFileDoesNotExist : public std::runtime_error
{
public:
	explicit XConfigFileDoesNotExist() : std::runtime_error("Config file does not exist!")
	{
	}

	explicit XConfigFileDoesNotExist(const std::string& path) : std::runtime_error("Config file at [" + path + "] does not exist!")
	{
	}
};

class XInvalidConfigParameter : public std::runtime_error
{
public:
   explicit XInvalidConfigParameter() : std::runtime_error("Invalid config parameter value!")
   {
   }

   explicit XInvalidConfigParameter(const std::string& name) : std::runtime_error("Invalid config parameter value for [" + name + "]!")
   {
   }
};

#endif // __CONFIG_EXCEPTIONS_H__