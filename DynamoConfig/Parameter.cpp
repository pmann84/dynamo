#include "Parameter.h"

CParameter::CParameter(std::string name) : m_name(name), m_value("")
{
}

CParameter::CParameter(std::string name, std::string value) : m_name(name), m_value(value)
{
}

std::string CParameter::Name() const
{
	return m_name;
}

std::string CParameter::Value() const
{
	return m_value;
}

CParameter::operator double() const
{
	if (m_value == "M_PI")
	{
		return M_PI;
	}
	return std::stof(m_value.c_str());
}

CParameter::operator std::string() const
{
	return m_value;
}

CParameter::operator int() const
{
	return std::stoi(m_value);
}

CParameter::operator unsigned() const
{
   return std::stoul(m_value);
}

CParameter::operator bool() const
{
	std::string lowerCaseBool(m_value);
	std::transform(m_value.begin(), m_value.end(), lowerCaseBool.begin(), ::tolower);
	std::istringstream istream(m_value);
	bool b;
	istream >> std::boolalpha >> b;
	return b;
}
