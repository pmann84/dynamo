#ifndef __BOUNDARY_CONDITIONS_H__
#define __BOUNDARY_CONDITIONS_H__

#include <string>

enum class BoundaryConditions
{
   Potential,
   Radial
};

BoundaryConditions parseBoundaryConditionConfig(const std::string condition);

#endif // __BOUNDARY_CONDITIONS_H__