﻿#ifndef __SIMULATION_H__
#define __SIMULATION_H__

#include "stdafx.h"

#include <memory>

#include "DerivUtils.h"
#include "Profiles.h"
#include "Logger.h"
#include "SimulationIO.h"
#include "../DynamoConfig/Config.h"
#include "ConfigValidator.h"

enum class PoloidalForcing
{
   Classical,
   BabcockLeighton
};

class CSimulation
{
public:
   CSimulation();
   ~CSimulation();

   bool Init(std::string configPath);
   void Run();

private:
   // Config and system objects
   std::string m_sConfigFilePath;

   CLogger m_logger;
   ConfigValidator m_config_validator;
   CConfig m_config;
   CConfig m_old_config;
   CSimulationIO m_simio;
   bool m_restart;
   std::string m_restart_directory;
   unsigned int m_restart_index;

   // Dependent variables
   std::unique_ptr<CVariable> m_r;
   std::unique_ptr<CVariable> m_sin_th;
   std::unique_ptr<CVariable> m_cos_th;
   std::unique_ptr<CVariable> m_th;

   // Static simulation profiles
   std::unique_ptr<Field1D> m_eta; // Diffusion
   std::unique_ptr<Field2D> m_alpha, m_bl_alpha; // Poloidal Source
   std::unique_ptr<Field2D> m_diffrot; // Differential rotation
   std::unique_ptr<Field2D> m_merid_stream, m_merid_ur, m_merid_uth; // Meridional flow

   // Evolving fields 
   std::unique_ptr<Field2D> m_an, m_bn;
   // Setup field components
   std::unique_ptr<Field2D> m_br, m_bth, m_bphi;

   void calculate_components_from_axisymmetric_fields();
   double calculate_energy();
   double calculate_parity();
};

#endif // __SIMULATION_H__