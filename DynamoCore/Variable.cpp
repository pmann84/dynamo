﻿#include "Variable.h"

CVariable::CVariable(int n, double start, double end) : CMesh1D(n), m_start(start), m_end(end), m_num_points(n)
{
   m_delta = (m_end - m_start) / (dim() - 1);
   initialise();
}

CVariable::~CVariable()
{
}

double CVariable::delta() const
{
   return m_delta;
}

int CVariable::num() const
{
   return m_num_points;
}

void CVariable::initialise()
{
   for (int i = 0; i < dim(); ++i)
   {
      (*this)(i) = m_start + i*m_delta;
   }
}
