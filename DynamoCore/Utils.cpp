﻿#include "Utils.h"

#include <sstream>

// Returns the current formatted time
std::string Utils::get_curr_time_string()
{
   return get_curr_time_string_format("%d-%m-%Y %X");
}

std::string Utils::get_file_friendly_curr_time_string()
{
   return get_curr_time_string_format("%d-%m-%Y_%H-%M-%S");
}

std::string Utils::get_curr_time_string_format(std::string format)
{
   std::time_t t = std::time(nullptr);
   struct tm tm;
   localtime_s(&tm, &t);
   std::stringstream ss;
   ss << std::put_time(&tm, format.c_str());
   return ss.str();
}

void Utils::print_time_info(int i, double dt)
{
   std::cout << std::setw(1) << std::left << "[" \
      << std::setw(19) << std::left << get_curr_time_string() \
      << std::setw(8) << std::left << "] step: " \
      << std::setw(6) << std::left << i \
      << std::setw(8) << std::left << "| time: " \
      << std::setw(15) << std::left << dt*i \
      << std::setw(3) << std::left << " | ";
}
