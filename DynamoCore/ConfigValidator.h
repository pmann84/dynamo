#ifndef __CONFIG_VALIDATOR_H__
#define __CONFIG_VALIDATOR_H__
#include "../DynamoConfig/IConfigValidator.h"
#include "../DynamoConfig/Config.h"

class ConfigValidator : public IConfigValidator
{
public:
   ConfigValidator();
   ~ConfigValidator();

   bool validate(const CConfig& config) override;
};

#endif // __CONFIG_VALIDATOR_H__
