﻿#include "Profiles.h"

// Test functions
double Profiles::InitSin1d(double x)
{
   return sin(x);
}

double Profiles::InitSin2d(double x, double y)
{
   return x*sin(y);
}

// Field initialisation functions
double Profiles::poloidal_initialisation(double x, double y)
{
   double interface_radius = 0.7;
   return 0.01*exp((-1.0*(x - interface_radius)*(x - interface_radius)) / 0.0025)*cos(y)*sin(y);
}

double Profiles::poloidal_initialisation_v2(double x, double y)
{
	if (x < 0.7)
	{
		return 0.0;
	}
	return sin(y) / (x*x);
}

// Profile functions
// Benchmark profile functions
double Profiles::differential_rotation_benchmark(double x, double y)
{
   double omega_c = 0.92;
   double r_c = 0.7;
   double d = 0.02;
   double c2 = 0.2;
   return omega_c + 0.5*(1.0 + erf((x - r_c) / d))*(1.0 - omega_c - c2*cos(y)*cos(y));
}

double Profiles::diffusion_benchmark(double x)
{
   double eta_c = 1000000000.0; // 1e9 
   double eta_t = 100000000000.0; // 1e11 => eta_c = 0.01*eta_t
   double r_c = 0.7;
   double d = 0.02;
   return (eta_c / eta_t) + 0.5*(1.0 - (eta_c / eta_t))*(1.0 + erf((x - r_c) / d));
}

double Profiles::alpha_benchmark(double x, double y)
{
   double r_c = 0.7;
   double d = 0.02;
   return 0.75*sqrt(3.0)*sin(y)*sin(y)*cos(y)*(1.0 + erf((x - r_c) / d));
}

double Profiles::babcock_leighton_benchmark(double x, double y)
{
   double r1 = 0.95;
   double r2 = 1.0;
   double d = 0.01;
   return 0.5*(1.0 + erf((x - r1) / d))*(1.0 - erf((x - r2) / d))*cos(y)*sin(y);
}

double Profiles::meridional_stream_benchmark(double x, double y)
{
   double rb = 0.65;
   double omrb = 1.0 - rb;
   double rmrb = x - rb;
   double pi = 3.14159;
   return ((-2.0*rmrb*rmrb) / (pi*omrb))*sin(pi*rmrb / omrb)*cos(y)*sin(y);
}

double Profiles::meridional_ur_benchmark(double x, double y)
{
   double rb = 0.65;
   double omrb = 1.0 - rb;
   double rmrb = x - rb;
   double pi = 3.14159;
   return ((-2.0*omrb*rmrb*rmrb) / (pi*x*omrb*omrb))*sin((pi*rmrb) / omrb)*(3.0*cos(y)*cos(y) - 1.0);
}

double Profiles::meridional_uth_benchmark(double x, double y)
{
   double rb = 0.65;
   double omrb = 1.0 - rb;
   double rmrb = x - rb;
   double pi = 3.14159;
   return ((((3.0*x - rb) / omrb)*sin((pi*rmrb) / omrb)) + (((x*pi*rmrb) / (omrb*omrb))*cos((pi*rmrb) / omrb)))*((2.0*omrb*rmrb) / (pi*x*omrb))*cos(y)*sin(y);
}