﻿#include "Field.h"
//******************************************************************************************//
// Field1D Implementation
//******************************************************************************************//
Field1D::Field1D(int n) : CMesh1D(n)
{
}

Field1D::~Field1D()
{
}

void Field1D::initialise(double(*initFn)(double), CVariable& v)
{
   for (int i = 0; i < dim(); ++i)
   {

      (*this)(i) = (*initFn)(v(i));
   }
}

//******************************************************************************************//
// Field2D Implementation
//******************************************************************************************//
Field2D::Field2D(int n1, int n2) : CMesh2D(n1, n2)
{
}

Field2D::Field2D(int n1, int n2, double val) : CMesh2D(n1, n2)
{
   initialise(val);
}

Field2D::Field2D(Field2D& f) : CMesh2D(f.dim(0), f.dim(1))
{
   for (int i = 0; i < dim(0); ++i)
   {
      for (int j = 0; j < dim(1); ++j)
      {
         (*this)(i, j) = f(i, j);
      }
   }
}

Field2D::~Field2D()
{
}

void Field2D::initialise(double val)
{
   for (int i = 0; i < dim(0); ++i)
   {
      for (int j = 0; j < dim(1); ++j)
      {
         (*this)(i, j) = val;
      }
   }
}
void Field2D::initialise(std::function<double(double, double)> init_fn, const CVariable &v1, const CVariable &v2)
{
   for (int i = 0; i < dim(0); ++i)
   {
      for (int j = 0; j < dim(1); ++j)
      {
         (*this)(i, j) = init_fn(v1(i), v2(j));
      }
   }
}

void Field2D::MultiplyByVariable(Field2D& f, const CVariable& v, int dimIdx)
{
   // TODO: Validate dimIdx here! Only 0 or 1 allowed
   // Check operands are the same size
   if (f.dim(dimIdx) != v.dim())
   {
      throw "Operands of Field2D operator and variable must be of the same size!";
   }
   // Create the new mesh
   int varIdx = -1;
   // Do the loop
   for (int i = 0; i < f.dim(0); ++i)
   {
      for (int j = 0; j < f.dim(1); ++j)
      {
         if (dimIdx == 0)
         {
            varIdx = i;
         }
         else if (dimIdx == 1)
         {
            varIdx = j;
         }
         f(i, j) *= v(varIdx);
      }
   }
}

void Field2D::operator=(const Field2D &f)
{
   for (int i = 0; i < dim(0); ++i)
   {
      for (int j = 0; j < dim(1); ++j)
      {
         (*this)(i, j) = f(i, j);
      }
   }
}

Field2D operator+(const Field2D &m1, const Field2D &m2)
{
   // Check operands are the same size
   if (m1.dim(0) != m2.dim(0) || (m1.dim(1) != m2.dim(1)))
   {
      throw "Operands of Mesh2D operator must be of the same size!";
   }
   // Create the new mesh
   Field2D new_mesh(m1.dim(0), m1.dim(1));
   // Do the loop
   for (int i = 0; i < new_mesh.dim(0); ++i)
   {
      for (int j = 0; j < new_mesh.dim(1); ++j)
      {
         new_mesh(i, j) = m1(i, j) + m2(i, j);
      }
   }
   return new_mesh;
}

Field2D operator+(double val, const Field2D &m1)
{
   // Create the new mesh
   Field2D new_mesh(m1.dim(0), m1.dim(1));
   // Do the loop
   for (int i = 0; i < new_mesh.dim(0); ++i)
   {
      for (int j = 0; j < new_mesh.dim(1); ++j)
      {
         new_mesh(i, j) = m1(i, j) + val;
      }
   }
   return new_mesh;
}

Field2D operator-(const Field2D &m1, const Field2D &m2)
{
   // Check operands are the same size
   if (m1.dim(0) != m2.dim(0) || (m1.dim(1) != m2.dim(1)))
   {
      throw "Operands of Mesh2D operator must be of the same size!";
   }
   // Create the new mesh
   Field2D new_mesh(m1.dim(0), m1.dim(1));
   // Do the loop
   for (int i = 0; i < new_mesh.dim(0); ++i)
   {
      for (int j = 0; j < new_mesh.dim(1); ++j)
      {
         new_mesh(i, j) = m1(i, j) - m2(i, j);
      }
   }
   return new_mesh;
}

Field2D operator*(const Field2D &m1, const Field2D &m2)
{
   // Check operands are the same size
   if (m1.dim(0) != m2.dim(0) || (m1.dim(1) != m2.dim(1)))
   {
      throw "Operands of Mesh2D operator must be of the same size!";
   }
   // Create the new mesh
   Field2D new_mesh(m1.dim(0), m1.dim(1));
   // Do the loop
   for (int i = 0; i < new_mesh.dim(0); ++i)
   {
      for (int j = 0; j < new_mesh.dim(1); ++j)
      {
         new_mesh(i, j) = m1(i, j)*m2(i, j);
      }
   }
   return new_mesh;
}

Field2D operator*(double val, const Field2D &m1)
{
   // Create the new mesh
   Field2D new_mesh(m1.dim(0), m1.dim(1));
   // Do the loop
   for (int i = 0; i < new_mesh.dim(0); ++i)
   {
      for (int j = 0; j < new_mesh.dim(1); ++j)
      {
         new_mesh(i, j) = m1(i, j)*val;
      }
   }
   return new_mesh;
}

Field2D operator/(const Field2D &m1, const Field2D &m2)
{
   // Check operands are the same size
   if (m1.dim(0) != m2.dim(0) || (m1.dim(1) != m2.dim(1)))
   {
      throw "Operands of Mesh2D operator must be of the same size!";
   }
   // Create the new mesh
   Field2D new_mesh(m1.dim(0), m1.dim(1));
   // Do the loop
   for (int i = 0; i < new_mesh.dim(0); ++i)
   {
      for (int j = 0; j < new_mesh.dim(1); ++j)
      {
         new_mesh(i, j) = m1(i, j) / m2(i, j);
      }
   }
   return new_mesh;
}