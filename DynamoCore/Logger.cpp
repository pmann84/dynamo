﻿#include "Logger.h"

CLogger::CLogger()
{
}

CLogger::~CLogger()
{
}

void CLogger::Log(std::string msg) const
{
   std::cout << GetLogString(msg) << std::endl;
}

void CLogger::LogTimestep(int i, double dt) const
{
   Log(GetTimestepLogString(i, dt));
}

void CLogger::LogTimestepAndEnergy(int i, double dt, double energy) const
{
   std::stringstream ss;
   ss << GetTimestepLogString(i, dt);
   ss << " energy = ";
   ss << std::setw(15) << std::left << energy;
   Log(ss.str());
}

void CLogger::LogMeshStatistics(std::string meshName, const CMesh2D& mesh) const
{
   Log(GetMeshStatsLogString(meshName, mesh));
}

std::string CLogger::GetLogString(std::string msg) const
{
   return "[" + Utils::get_curr_time_string() + "] " + msg;
}

std::string CLogger::GetTimestepLogString(int i, double dt) const
{
   std::stringstream ss;
   ss << "step: " \
      << std::setw(6) << std::left << i \
      << std::setw(8) << std::left << "| time: " \
      << std::setw(15) << std::left << dt*i \
      << std::setw(3) << std::left << " | ";
   return ss.str();
}

std::string CLogger::GetMeshStatsLogString(std::string meshName, const CMesh2D& mesh) const
{
   std::stringstream ss;
   ss << meshName << ": " << mesh.getMeshStats();
   return ss.str();
}
