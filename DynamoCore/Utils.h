﻿#ifndef __UTILS_H__
#define __UTILS_H__

#include <string>
#include <iostream>
#include <iomanip>

namespace Utils
{
   std::string get_curr_time_string();
   std::string get_file_friendly_curr_time_string();
   std::string get_curr_time_string_format(std::string format);
   void print_time_info(int i, double dt);
}

#endif