#pragma once

#include <vector>

class IMesh
{
public:
   virtual ~IMesh() {}

   virtual std::vector<double>::iterator minval() = 0;
   virtual std::vector<double>::iterator maxval() = 0;
   virtual std::vector<double>::const_iterator minval() const = 0;
   virtual std::vector<double>::const_iterator maxval() const = 0;

   virtual bool validate() = 0;
   virtual void print_mesh() = 0;
};