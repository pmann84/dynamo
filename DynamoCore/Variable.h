﻿#ifndef __VARIABLE_H__
#define __VARIABLE_H__

#include "Mesh.h"

class CVariable : public CMesh1D
{
public:
   CVariable(int n, double start, double end);
   ~CVariable();

   double delta() const;
   int num() const;
private:
   void initialise();
   double m_start, m_end, m_delta;
   int m_num_points;
};

#endif