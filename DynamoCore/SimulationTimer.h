#ifndef __SIMULATION_TIMER_H__
#define __SIMULATION_TIMER_H__

#include <chrono>
#include <string>

class SimulationTimer
{
public:
   SimulationTimer();
   ~SimulationTimer();

   void start();
   void finish();

   double elapsed_milliseconds() const;
   double elapsed_seconds() const;
   std::string get_elapsed_time_string() const;

private:
   std::chrono::time_point<std::chrono::system_clock> get_finish_time() const;

   bool m_is_running;
   std::chrono::time_point<std::chrono::system_clock> m_start;
   std::chrono::time_point<std::chrono::system_clock> m_finish;
};

#endif // __SIMULATION_TIMER_H__
