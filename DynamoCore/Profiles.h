﻿#ifndef __PROFILES_H__
#define __PROFILES_H__

#include <math.h>

namespace Profiles
{
   // Test functions
   double InitSin1d(double x);
   double InitSin2d(double x, double y);

   // Field initialisation functions
   double poloidal_initialisation(double x, double y);
   double poloidal_initialisation_v2(double x, double y);

   // Profile functions
   // Benchmark profiles
   double differential_rotation_benchmark(double x, double y);
   double diffusion_benchmark(double x);
   double alpha_benchmark(double x, double y);
   double babcock_leighton_benchmark(double x, double y);
   double meridional_stream_benchmark(double x, double y);
   double meridional_ur_benchmark(double x, double y);
   double meridional_uth_benchmark(double x, double y);
}

#endif // __PROFILES_H__