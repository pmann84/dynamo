﻿#include "mesh.h"

#include <iostream>
#include <sstream>
#include <algorithm>
#include <iomanip>

//******************************************************************************************//
// CMesh1D Implementation
//******************************************************************************************//
CMesh1D::CMesh1D(int n) : m_n(n), m_mesh(m_n)
{
}

int CMesh1D::dim() const
{
   return m_n;
}

std::vector<double>::iterator CMesh1D::minval()
{
   return std::min_element(m_mesh.begin(), m_mesh.end());
}

std::vector<double>::iterator CMesh1D::maxval()
{
   return std::max_element(m_mesh.begin(), m_mesh.end());
}

std::vector<double>::const_iterator CMesh1D::minval() const
{
   return std::min_element(m_mesh.begin(), m_mesh.end());
}

std::vector<double>::const_iterator CMesh1D::maxval() const
{
   return std::max_element(m_mesh.begin(), m_mesh.end());
}

double CMesh1D::operator()(const int i) const
{
   return m_mesh[i];
}

double& CMesh1D::operator()(const int i)
{
   return m_mesh[i];
}

bool CMesh1D::validate()
{
   return m_n == m_mesh.size();
}

void CMesh1D::print_mesh()
{
   for (auto it = m_mesh.begin(); it != m_mesh.end(); ++it)
   {
      std::cout << (*it) << " ";
   }
   std::cout << std::endl;
}

//******************************************************************************************//
// CMesh2D Implementation
//******************************************************************************************//
CMesh2D::CMesh2D(int n1, int n2) : m_n1(n1), m_n2(n2), m_mesh(m_n1*m_n2)
{
}

int CMesh2D::dim(int d) const
{
   if (d == 0)
   {
      return m_n1;
   }
   else if (d == 1)
   {
      return m_n2;
   }
   throw;
}

std::vector<double>::iterator CMesh2D::minval()
{
   return std::min_element(m_mesh.begin(), m_mesh.end());
}

std::vector<double>::iterator CMesh2D::maxval()
{
   return std::max_element(m_mesh.begin(), m_mesh.end());
}

std::vector<double>::const_iterator CMesh2D::minval() const
{
   return std::min_element(m_mesh.begin(), m_mesh.end());
}

std::vector<double>::const_iterator CMesh2D::maxval() const
{
   return std::max_element(m_mesh.begin(), m_mesh.end());
}

bool CMesh2D::validate()
{
   return m_n1*m_n2 == m_mesh.size();
}

void CMesh2D::print_mesh()
{
   for (int i = 0; i < m_n1; ++i)
   {
      for (int j = 0; j < m_n2; ++j)
      {
         std::cout << (*this)(i, j) << " ";
      }
      std::cout << std::endl;
   }
}

std::string CMesh2D::getMeshStats() const
{
   std::stringstream ss;
   auto max_double_precision = 12;
   ss << std::setw(8) << std::left << "min = " \
      << std::setw(max_double_precision + 7) << std::setprecision(max_double_precision) << std::left << *minval() \
      << std::setw(8) << std::left << ", max = " \
      << std::setw(max_double_precision + 7) << std::setprecision(max_double_precision) << std::left << *maxval() \
      << std::setw(3) << std::left << " | ";
   return ss.str();
}

double CMesh2D::operator()(const int i, const int j) const
{
   return m_mesh[i*m_n2 + j];
}

double& CMesh2D::operator()(const int i, const int j)
{
   return m_mesh[i*m_n2 + j];
}

void CMesh2D::operator=(const CMesh2D &m)
{
   for (int i = 0; i < m_n1; ++i)
   {
      for (int j = 0; j < m_n2; ++j)
      {
         (*this)(i, j) = m(i, j);
      }
   }
}

CMesh2D operator+(const CMesh2D &m1, const CMesh2D &m2)
{
   // Check operands are the same size
   if ((m1.m_n1 != m2.m_n1) || (m1.m_n2 != m2.m_n2))
   {
      throw "Operands of CMesh2D operator must be of the same size!";
   }
   // Create the new mesh
   CMesh2D new_mesh(m1.m_n1, m1.m_n2);
   // Do the loop
   for (int i = 0; i < new_mesh.m_n1; ++i)
   {
      for (int j = 0; j < new_mesh.m_n2; ++j)
      {
         new_mesh(i, j) = m1(i, j) + m2(i, j);
      }
   }
   return new_mesh;
}

CMesh2D operator-(const CMesh2D &m1, const CMesh2D &m2)
{
   // Check operands are the same size
   if ((m1.m_n1 != m2.m_n1) || (m1.m_n2 != m2.m_n2))
   {
      throw "Operands of CMesh2D operator must be of the same size!";
   }
   // Create the new mesh
   CMesh2D new_mesh(m1.m_n1, m1.m_n2);
   // Do the loop
   for (int i = 0; i < new_mesh.m_n1; ++i)
   {
      for (int j = 0; j < new_mesh.m_n2; ++j)
      {
         new_mesh(i, j) = m1(i, j) - m2(i, j);
      }
   }
   return new_mesh;
}

CMesh2D operator*(const CMesh2D &m1, const CMesh2D &m2)
{
   // Check operands are the same size
   if ((m1.m_n1 != m2.m_n1) || (m1.m_n2 != m2.m_n2))
   {
      throw "Operands of CMesh2D operator must be of the same size!";
   }
   // Create the new mesh
   CMesh2D new_mesh(m1.m_n1, m1.m_n2);
   // Do the loop
   for (int i = 0; i < new_mesh.m_n1; ++i)
   {
      for (int j = 0; j < new_mesh.m_n2; ++j)
      {
         new_mesh(i, j) = m1(i, j)*m2(i, j);
      }
   }
   return new_mesh;
}

CMesh2D operator*(double val, const CMesh2D &m1)
{
   // Create the new mesh
   CMesh2D new_mesh(m1.m_n1, m1.m_n2);
   // Do the loop
   for (int i = 0; i < new_mesh.m_n1; ++i)
   {
      for (int j = 0; j < new_mesh.m_n2; ++j)
      {
         new_mesh(i, j) = m1(i, j) + val;
      }
   }
   return new_mesh;
}