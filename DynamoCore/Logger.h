﻿#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <string>
#include <iostream>
#include <sstream>
#include "Mesh.h"
#include "Utils.h"

class CLogger
{
public:
   explicit CLogger();
   ~CLogger();

   void Log(std::string msg) const;
   void LogTimestep(int i, double dt) const;
   void LogTimestepAndEnergy(int i, double dt, double energy) const;
   void LogMeshStatistics(std::string meshName, const CMesh2D& mesh) const;

private:
   std::string GetLogString(std::string msg) const;
   std::string GetTimestepLogString(int i, double dt) const;
   std::string GetMeshStatsLogString(std::string meshName, const CMesh2D& mesh) const;
};

#endif // __LOGGER_H__