#include "SimulationTimer.h"
#include <iostream>
#include <iomanip>
#include <sstream>

SimulationTimer::SimulationTimer() : m_is_running(false)
{
}

SimulationTimer::~SimulationTimer()
{
}

void SimulationTimer::start()
{
   m_is_running = true;
   m_start = std::chrono::system_clock::now();
}

void SimulationTimer::finish()
{
   m_is_running = false;
   m_finish = std::chrono::system_clock::now();
}

double SimulationTimer::elapsed_milliseconds() const
{
   std::chrono::time_point<std::chrono::system_clock> finish_time = get_finish_time();
   return std::chrono::duration_cast<std::chrono::milliseconds>(finish_time - m_start).count();
}

double SimulationTimer::elapsed_seconds() const
{
   return elapsed_milliseconds() / 1000.0;
}

std::string SimulationTimer::get_elapsed_time_string() const
{
   std::chrono::time_point<std::chrono::system_clock> finish_time = get_finish_time();
   auto duration = finish_time - m_start;
   auto hours = std::chrono::duration_cast<std::chrono::hours>(duration);
   auto minutes = std::chrono::duration_cast<std::chrono::minutes>(duration - hours);
   auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration - minutes);

   std::tm t{};

   t.tm_hour = hours.count();
   t.tm_min = minutes.count();
   t.tm_sec = seconds.count();
   std::stringstream ss;
   ss << std::put_time(&t, "%H:%M:%S");
   return ss.str();
}

std::chrono::time_point<std::chrono::system_clock> SimulationTimer::get_finish_time() const
{
   std::chrono::time_point<std::chrono::system_clock> finish_time;
   if (m_is_running)
   {
      finish_time = std::chrono::system_clock::now();
   }
   else
   {
      finish_time = m_finish;
   }
   return finish_time;
}
