﻿#include "SimulationIO.h"

#include <regex>

// Constructor for a fresh simulation
CSimulationIO::CSimulationIO() : m_field_out_count(0)
                               , m_prof_int(0)
                               , m_prof_start(0)
                               , m_prof_end(0)
                               , m_butt_int(0)
                               , m_butt_start(0)
                               , m_butt_end(0)
                               , m_calc_int(0)
                               , m_calc_start(0)
                               , m_calc_end(0)
{
}

CSimulationIO::~CSimulationIO()
{
}

bool CSimulationIO::Init(CConfig& config, std::string param_file, CLogger& logger)
{
   // Assign the logger
   m_logger = logger;

   std::string ca_str, cw_str, cs_str, ru_str;
   try
   {
      m_prof_int = config.getParamByName("profint");
      m_prof_start = config.getParamByName("profstart");
      m_prof_end = config.getParamByName("profend");
      m_butt_int = config.getParamByName("buttint");
      m_butt_start = config.getParamByName("buttstart");
      m_butt_end = config.getParamByName("buttend");
      m_calc_int = config.getParamByName("calcint");
      m_calc_start = config.getParamByName("calcstart");
      m_calc_end = config.getParamByName("calcend");
      m_output_dir = config.getParamByName("simoutput").Value();
      ca_str = config.getParamByName("calpha").Value();
      cw_str = config.getParamByName("comega").Value();
      cs_str = config.getParamByName("cs").Value();
      ru_str = config.getParamByName("ru").Value();

   }
   catch (XConfigParameterDoesNotExist e)
   {
      m_logger.Log(e.what());
      throw e;
   }

   // Create the directory in the output directory for writing
   const auto output_dir_path = fs::path(m_output_dir);
   if (fs::exists(output_dir_path))
   {
      m_logger.Log("Directory " + m_output_dir + " already exists, skipping.");
   }
   else
   {
      fs::create_directory(output_dir_path);
   }

   // Create a folder inside the output_dir to hold all the 
   // data generated from the run
   std::string timestring = Utils::get_file_friendly_curr_time_string();
   std::string run_dir = timestring + "_Ca=" + ca_str + "_Cs=" + cs_str + "_Cw=" + cw_str + "_Ru=" + ru_str;
   run_dir = m_output_dir + "\\" + run_dir;
   m_logger.Log("Creating directory for simulation: " + run_dir);
   const auto run_dir_path = fs::path(run_dir);
   if (fs::exists(run_dir_path))
   {
      m_logger.Log("Directory " + run_dir + " already exists, skipping.");
   }
   else
   {
      fs::create_directory(run_dir_path);
   }
   m_run_dir = run_dir;

   // Finally copy the param file to the output location
   std::string dest_param_path = m_run_dir + "\\params.dat";

   const auto src_path = fs::path(param_file);
   const auto dst_path = fs::path(dest_param_path);
   fs::copy(src_path, dst_path);
   m_logger.Log("Parameter file (" + param_file + ") copied to output directory: " + m_run_dir);
   return true;
}

// Constructor for restarting a simulation
bool CSimulationIO::Init(CConfig& old_config, CConfig& new_config, CLogger& logger)
{
   // Assign the logger
   m_logger = logger;

   // Assign the temporal parameters from old config
   try
   {
      m_prof_int = old_config.getParamByName("profint");
      m_prof_start = old_config.getParamByName("profstart");
      m_prof_end = old_config.getParamByName("profend");
      m_butt_int = old_config.getParamByName("buttint");
      m_butt_start = old_config.getParamByName("buttstart");
      m_butt_end = old_config.getParamByName("buttend");
      m_calc_int = old_config.getParamByName("calcint");
      m_calc_start = old_config.getParamByName("calcstart");
      m_calc_end = old_config.getParamByName("calcend");
      m_output_dir = old_config.getParamByName("simoutput").Value();
   }
   catch (XConfigParameterDoesNotExist e)
   {
      m_logger.Log(e.what());
      throw e;
   }

   // Set the run dir - this is just the restart directory
   m_run_dir = new_config.getParamByName("restart_directory");

   // Set the output count to be one after the last one
   std::regex re("a([0-9]+)");
   fs::recursive_directory_iterator end_itr; // default construction yields past-the-end
   for (fs::recursive_directory_iterator itr{fs::path(m_run_dir)}; itr != end_itr; ++itr)
   {
      std::string filename = itr->path().stem().string();
      std::smatch matches;
      if (std::regex_search(filename, matches, re))
      {
         int num_matches = matches.size();
         if (num_matches == 2)
         {
            int file_index = std::atoi(matches[1].str().c_str());
            if (file_index > m_field_out_count)
            {
               m_field_out_count = file_index;
            }
         }
      }
   }
   // Increment by 1 to get the next one
   m_field_out_count++;
   m_logger.Log("Starting output at index " + std::to_string(m_field_out_count));
}

void CSimulationIO::WriteDiffusivityProfiles(const CVariable& r, const Field1D& eta, const Field1D& deta)
{
   m_logger.Log("Writing diffusion and derivative to file... ");
   WriteData(eta, r, "eta.txt");
   WriteData(deta, r, "deta.txt");
   m_logger.Log("Writing complete!");
}

void CSimulationIO::WriteRotationProfiles(const CVariable& r, const CVariable& th, const Field2D& diffrot, const Field2D& rad_shear, const Field2D& lat_shear)
{
   m_logger.Log("Writing Differential Rotation and derivative to file...");
   WriteData(diffrot, r, th, "diffrot.txt");
   WriteData(rad_shear, r, th, "radshear.txt");
   WriteData(lat_shear, r, th, "latshear.txt");
   m_logger.Log("Writing complete!");
}

void CSimulationIO::WriteCirculationProfiles(const CVariable& r, const CVariable& th, const Field2D& ur, const Field2D& uth)
{
   m_logger.Log("Writing Meridional Flow and Stream Function to file...");
   WriteData(ur, r, th, "ur.txt");
   WriteData(uth, r, th, "uth.txt");
   m_logger.Log("Writing complete!");
}

void CSimulationIO::WriteSourceProfiles(const CVariable& r, const CVariable& th, const Field2D& source, const Field2D& bl_source)
{
   m_logger.Log("Writing Alpha Profile to file...");
   WriteData(source, r, th, "alpha.txt");
   WriteData(bl_source, r, th, "alpha_bl.txt");
   m_logger.Log("Writing complete!");
}

void CSimulationIO::WriteMagneticField(const CVariable& r, const CVariable& th, const Field2D& a, const Field2D& b, const Field2D& br, const Field2D& bth, const Field2D& bphi)
{
   //  Output given field profiles
   //m_logger.Log("Writing magnetic field snapshots to file: ");

   WriteData(a, r, th, "a" + std::to_string(m_field_out_count) + ".txt");
   WriteData(b, r, th, "b" + std::to_string(m_field_out_count) + ".txt");
   WriteData(br, r, th, "br" + std::to_string(m_field_out_count) + ".txt");
   WriteData(bth, r, th, "bth" + std::to_string(m_field_out_count) + ".txt");
   WriteData(bphi, r, th, "bphi" + std::to_string(m_field_out_count) + ".txt");

   //m_logger.Log("Writing complete!");
   m_field_out_count++;
}

void CSimulationIO::WriteMagneticField(const CVariable& r, const CVariable& th, const Field2D& a, const Field2D& b)
{
   //  Output given field profiles
   //m_logger.Log("Writing magnetic field snapshots to file: ");

   WriteData(a, r, th, "a" + std::to_string(m_field_out_count) + ".txt");
   WriteData(b, r, th, "b" + std::to_string(m_field_out_count) + ".txt");

   //m_logger.Log("Writing complete!");
   //m_logger.Log("Field snapshots index " + std::to_string(m_field_out_count) + " written to file.");
   m_field_out_count++;
}

void CSimulationIO::WriteTimeParameters(CConfig& config)
{
   std::ofstream outfile;
   const std::string full_filepath = m_run_dir + "\\time.txt";
   outfile.open(full_filepath);

   outfile << config.getParamByName("nt").Value() << std::endl;
   outfile << config.getParamByName("dt").Value() << std::endl;

   outfile << config.getParamByName("buttstart").Value() << std::endl;
   outfile << config.getParamByName("buttend").Value() << std::endl;
   outfile << config.getParamByName("buttint").Value() << std::endl;

   outfile << config.getParamByName("profstart").Value() << std::endl;
   outfile << config.getParamByName("profend").Value() << std::endl;
   outfile << config.getParamByName("profint").Value() << std::endl;

   outfile << config.getParamByName("calcstart").Value() << std::endl;
   outfile << config.getParamByName("calcend").Value() << std::endl;
   outfile << config.getParamByName("calcint").Value() << std::endl;

   outfile.close();
}

void CSimulationIO::AppendButterfly(const Field2D& f, const unsigned int idx, const std::string filename) const
{
   std::ofstream outfile;
   const std::string full_filepath = m_run_dir + "\\" + filename;
   outfile.open(full_filepath, std::fstream::in | std::fstream::out | std::fstream::app);
   const int dim = f.dim(1);
   for (auto j = 0; j < dim; ++j)
   {
      outfile << f(idx, j) << space_delim;
   }
   outfile << std::endl;
   outfile.close();
}

void CSimulationIO::AppendData(double val, std::string filename)
{
   std::ofstream outfile;
   const std::string full_filepath = m_run_dir + "\\" + filename;
   outfile.open(full_filepath, std::fstream::in | std::fstream::out | std::fstream::app);
   outfile << val;
   outfile << std::endl;
   outfile.close();
}

void CSimulationIO::WriteData(CMesh1D m, CVariable v, std::string filename)
{
   // To plot in gnuplot the data needs to be written to file as 
   // x0 f(x0)
   // ..  ..
   // xn f(xn)
   std::ofstream outfile;
   std::string full_filepath = m_run_dir + "\\" + filename;
   outfile.open(full_filepath);
   for (auto i = 0; i < v.dim(); ++i)
   {
      outfile << v(i) << space_delim << m(i) << std::endl;
   }
   outfile.close();
}

void CSimulationIO::WriteData(CMesh2D m, CVariable v1, CVariable v2, std::string filename)
{
   // To plot in gnuplot the data needs to be written to file as 
   // follows with a newline inbetween changing y values
   // x0 y0   f(x0,y0)
   // x1 y0   f(x1,y0)
   // x2 y0   f(x2,y0)
   // .. ..      ..
   // xn y0   f(xn,y0)
   //
   // x0 y1   f(x0,y1)
   // .. ..      ..
   // xn y1   f(xn,y1)
   //
   // x0 ym   f(x0,ym)
   // xn ym   f(xn,ym)
   // The polar keyword can be used if you are outputting a polar grid
   // this converts it to cartesian so it displays correctly in gnuplot
   // it converts using the 
   std::ofstream outfile;
   std::string full_filepath = m_run_dir + "\\" + filename;
   outfile.open(full_filepath);

   for (int i = 0; i < v1.dim(); ++i)
   {
      for (int j = 0; j < v2.dim(); ++j)
      {
         outfile << v1(i) << space_delim << v2(j) << space_delim << m(i, j) << std::endl;
      }
      outfile << std::endl;
   }

   outfile.close();
}

void CSimulationIO::ReadData(const fs::path filepath, CMesh1D &m)
{
   if (!fs::exists(filepath))
   {
      // Stop here as parameters arent valid
     throw std::runtime_error("Path to " + filepath.string() + " does not exist!");
   }
   std::ifstream infile;
   std::string line;
   infile.open(filepath);
   int idx = 0;
   if (infile.is_open())
   {
      while (getline(infile, line))
      {
         std::vector<std::string> split_str;
         StringUtils::split(line, space_delim, split_str);
         m(idx) = static_cast<double>(atof(split_str[1].c_str()));
         idx++;
      }
      infile.close();
   }
}

void CSimulationIO::ReadData(const fs::path filepath, CMesh2D &m)
{
   if (!fs::exists(filepath))
   {
      // Stop here as parameters arent valid
      throw std::runtime_error("Path to " + filepath.string() + " does not exist!");
   }
   std::ifstream infile;
   std::string line;
   infile.open(filepath);
   int r_data_counter = 0;
   int th_data_counter = 0;
   if (infile.is_open())
   {
      while (getline(infile, line))
      {
         std::vector<std::string> split_str;
         StringUtils::split(line, space_delim, split_str);
         if (split_str.size() > 1)
         {
            std::string data_val = split_str[2];
            if (data_val == "-nan(ind)")
            {
               m(r_data_counter, th_data_counter) = 0.0;
            }
            else
            {
               m(r_data_counter, th_data_counter) = std::atoi(data_val.c_str());
            }
            th_data_counter++;
         }
         else
         {
            r_data_counter++;
            th_data_counter = 0;
         }
      }
      infile.close();
   }
}

bool CSimulationIO::ShouldWriteFields(const unsigned int t) const
{
   return t >= m_prof_start && t <= m_prof_end && t % m_prof_int == 0;
}

bool CSimulationIO::ShouldWriteButterflies(const unsigned int t) const
{
   return t >= m_butt_start && t <= m_butt_end && t % m_butt_int == 0;
}

bool CSimulationIO::ShouldWriteQuantity(const unsigned t) const
{
   return t >= m_calc_start && t <= m_calc_end && t % m_calc_int == 0;
}
