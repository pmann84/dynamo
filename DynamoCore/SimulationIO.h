﻿#ifndef __SIMULATIONIO_H__
#define __SIMULATIONIO_H__

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <stdlib.h> 
#include <Windows.h>

#include "Mesh.h"
#include "Field.h"
#include "../DynamoConfig/Config.h"
#include "Variable.h"
#include "Logger.h"
#include "Utils.h"

#include <filesystem>

namespace fs = std::experimental::filesystem;

const std::string space_delim(" ");

class CSimulationIO
{
public:
   CSimulationIO();
   ~CSimulationIO();

   bool Init(CConfig& config, std::string param_file, CLogger& logger);
   bool Init(CConfig& old_config, CConfig& new_config, CLogger& logger);

   void WriteDiffusivityProfiles(const CVariable& r, const Field1D& eta, const Field1D& deta);
   void WriteRotationProfiles(const CVariable& r, const CVariable& th, const Field2D& diffrot, const Field2D& rad_shear, const Field2D& lat_shear);
   void WriteCirculationProfiles(const CVariable& r, const CVariable& th, const Field2D& ur, const Field2D& uth);
   void WriteSourceProfiles(const CVariable& r, const CVariable& th, const Field2D& source, const Field2D& bl_source);

   void WriteMagneticField(const CVariable& r, const CVariable& th, const Field2D& a, const Field2D& b, const Field2D& br, const Field2D& bth, const Field2D& bphi);
   void WriteMagneticField(const CVariable& r, const CVariable& th, const Field2D& a, const Field2D& b);
   void WriteTimeParameters(CConfig& config);
   
   void AppendButterfly(const Field2D& f, const unsigned int idx, const std::string filename) const;
   void AppendData(double m, std::string filename);

   void WriteData(CMesh1D m, CVariable v, std::string filename);
   void WriteData(CMesh2D m, CVariable v1, CVariable v2, std::string filename);

   static void ReadData(const fs::path filepath, CMesh1D &m);
   static void ReadData(const fs::path filepath, CMesh2D &m);

   bool ShouldWriteFields(const unsigned int t) const;
   bool ShouldWriteButterflies(const unsigned int t) const;
   bool ShouldWriteQuantity(const unsigned int t) const;

private:
   unsigned int m_field_out_count;
   std::string m_output_dir;
   std::string m_run_dir;
   CLogger m_logger;

   unsigned int m_prof_int;
   unsigned int m_prof_start;
   unsigned int m_prof_end;
   unsigned int m_butt_int;
   unsigned int m_butt_start;
   unsigned int m_butt_end;
   unsigned int m_calc_int;
   unsigned int m_calc_start;
   unsigned int m_calc_end;
};



#endif