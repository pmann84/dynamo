﻿#define __STDCPP_WANT_MATH_SPEC_FUNCS__

#include "Simulation.h"
#include "SimulationTimer.h"
#include "BoundaryConditions.h"
#include "AssociatedLegendre.h"

#include <cmath>
#include <filesystem>

namespace fs = std::experimental::filesystem;

CSimulation::CSimulation() : m_config(m_config_validator), m_old_config(m_config_validator)
{
}

CSimulation::~CSimulation()
{
}

bool CSimulation::Init(std::string configPath)
{
   // Read in config - this does validation at the same time
   m_sConfigFilePath = configPath;
   if (!m_config.Read(m_sConfigFilePath))
   {
      // Stop here as parameters arent valid
      m_logger.Log("Invalid Parameters and/or values specified!");
      return false;
   }

   // Check here for a restart
   m_restart = m_config.getParamByName("restart");
   // Validate the restart parameters first
   if (m_restart)
   {
      m_restart_directory = m_config.getParamByName("restart_directory");
      m_restart_index = m_config.getParamByName("restart_index");
      // Check directory exists
      const auto restart_directory_path = fs::path(m_restart_directory);
      if (!fs::exists(restart_directory_path))
      {
         // Stop here as parameters arent valid
         m_logger.Log("Invalid restart directory. Does not exist!");
         return false;
      }

      // Check that the old config exists
      const auto old_config_file_path = fs::path(m_restart_directory + "\\params.dat");
      if (!fs::exists(restart_directory_path))
      {
         // Stop here if file doesnt exist
         m_logger.Log("Invalid restart directory. Does not exist!");
         return false;
      }
      // ... then load it
      if (!m_old_config.Read(old_config_file_path.string()))
      {
         // Stop here as parameters arent valid
         m_logger.Log("Invalid Parameters and/or values specified in old config!");
         return false;
      }
   }

   // Get the right config for the domain parameters
   CConfig& config_for_domain = m_restart ? m_old_config : m_config;

   // Get some useful params
   // Number of points in r
   int nr = config_for_domain.getParamByName("nr");
   // Number of points in theta
   int nth = config_for_domain.getParamByName("nth");
   // r range
   double rin = config_for_domain.getParamByName("rin");
   double rout = config_for_domain.getParamByName("rout");
   // theta range
   double thin = config_for_domain.getParamByName("thin");
   double thout = config_for_domain.getParamByName("thout");

   // Setup the variables
   m_r = std::make_unique<CVariable>(nr, rin, rout);
   m_th = std::make_unique<CVariable>(nth, thin, thout);
   m_sin_th = std::make_unique<CVariable>(nth, thin, thout);
   m_cos_th = std::make_unique<CVariable>(nth, thin, thout);
   for (int i = 0; i < nth; ++i)
   {
      (*m_sin_th)(i) = sin((*m_sin_th)(i));
      (*m_cos_th)(i) = cos((*m_cos_th)(i));
   }

   // Setup static profile arrays
   m_eta = std::make_unique<Field1D>(nr); // Diffusion
   m_alpha = std::make_unique<Field2D>(nr, nth, 0.0); // Poloidal Source
   m_bl_alpha = std::make_unique<Field2D>(nr, nth, 0.0); // BL Poloidal Source
   m_diffrot = std::make_unique<Field2D>(nr, nth, 0.0); // Differential rotation
   m_merid_stream = std::make_unique<Field2D>(nr, nth, 0.0); // Meridional flow stream function
   m_merid_ur = std::make_unique<Field2D>(nr, nth, 0.0); // Radial Meridional flow
   m_merid_uth = std::make_unique<Field2D>(nr, nth, 0.0); // Latitudinal Meridional flow

   // Populate profiles
   if (m_restart)
   {
      // TODO: ALL THE 2D FIELDS ARE GETTING READ IN INCORRECTLY - THE READ NEEDS TO CHANGE AS THE WRITING WAS CHANGED
      // Check and load all the profiles
      // Diffusion
      const auto existing_eta_filepath = fs::path(m_restart_directory + "\\eta.txt");
      CSimulationIO::ReadData(existing_eta_filepath.string(), *m_eta);
      // Sources      
      const auto existing_alpha_filepath = fs::path(m_restart_directory + "\\alpha.txt");
      CSimulationIO::ReadData(existing_alpha_filepath, *m_alpha);
      const auto existing_alpha_bl_filepath = fs::path(m_restart_directory + "\\alpha_bl.txt");
      CSimulationIO::ReadData(existing_alpha_bl_filepath, *m_bl_alpha);
      // Rotation
      const auto existing_omega_filepath = fs::path(m_restart_directory + "\\diffrot.txt");
      CSimulationIO::ReadData(existing_omega_filepath, *m_diffrot);
      // Circulation
      const auto existing_ur_filepath = fs::path(m_restart_directory + "\\ur.txt");
      CSimulationIO::ReadData(existing_ur_filepath, *m_merid_ur);
      const auto existing_uth_filepath = fs::path(m_restart_directory + "\\uth.txt");
      CSimulationIO::ReadData(existing_uth_filepath, *m_merid_uth);
   }
   else
   {
      //	- diffusion (and its derivative)
      m_eta->initialise(Profiles::diffusion_benchmark, *m_r);
      //	- alpha
      m_alpha->initialise(Profiles::alpha_benchmark, *m_r, *m_th);
      m_bl_alpha->initialise(Profiles::babcock_leighton_benchmark, *m_r, *m_th);
      //	- differential rotation
      m_diffrot->initialise(Profiles::differential_rotation_benchmark, *m_r, *m_th);
      //	- meridional flow
      // - Set stream function - calculate ur, and uth
      //m_merid_stream->initialise(Profiles::meridional_stream_benchmark, *m_r, *m_th);
      // - Otherwise set ur and uth explicitly
      m_merid_ur->initialise(Profiles::meridional_ur_benchmark, *m_r, *m_th);
      m_merid_uth->initialise(Profiles::meridional_uth_benchmark, *m_r, *m_th);
   }

   // Setup magnetic field arrays
   m_an = std::make_unique<Field2D>(nr, nth, 0.0);
   m_bn = std::make_unique<Field2D>(nr, nth, 0.0);
   
   // Initialise magnetic fields
   if (m_restart)
   {
      // Check that all the fields exist - A & B and load them
      CVariable r(nr, rin, rout), th(nth, thin, thout);
      const auto existing_a_filepath = fs::path(m_restart_directory + "\\a" + std::to_string(m_restart_index) + ".txt");
      CSimulationIO::ReadData(existing_a_filepath.string(), *m_an);
      const auto existing_b_filepath = fs::path(m_restart_directory + "\\b" + std::to_string(m_restart_index) + ".txt");
      CSimulationIO::ReadData(existing_b_filepath.string(), *m_bn);
   }
   else
   {
      //m_an->initialise(Profiles::poloidal_initialisation, *m_r, *m_th);
      m_bn->initialise(Profiles::poloidal_initialisation, *m_r, *m_th);
   }

   // Calculate field components
   m_br = std::make_unique<Field2D>(nr, nth, 0.0);
   m_bth = std::make_unique<Field2D>(nr, nth, 0.0);
   m_bphi = std::make_unique<Field2D>(nr, nth, 0.0);
   calculate_components_from_axisymmetric_fields();

   return true;
}

void CSimulation::Run()
{
   SimulationTimer timer;
   // Calculate associated legendre polynomials for l = 1:40 (40 x nth)
   const int ldegree = 40;
   std::vector<std::vector<double>> legendre(40, std::vector<double>(m_th->num()));
   std::vector<double> cos_th_vals;
   for (int i = 0; i < m_th->num(); ++i)
   {
      cos_th_vals.push_back(cos(m_th->operator()(i)));
   }
   for (int degree = 0; degree < ldegree; ++degree)
   {
      AssociatedLegendre leg(degree+1, 1);
      std::vector<double> leg_vals = leg(cos_th_vals);
      legendre[degree] = leg_vals;
   }

   Field2D brdiff(m_r->num(), m_th->num(), 0.0), bldiff(m_r->num(), m_th->num(), 0.0);
   Field2D ardiff(m_r->num(), m_th->num(), 0.0), aldiff(m_r->num(), m_th->num(), 0.0);
   Field2D alpha_class(m_r->num(), m_th->num(), 0.0), alpha_bl(m_r->num(), m_th->num(), 0.0);
   Field2D blshear(m_r->num(), m_th->num(), 0.0), brshear(m_r->num(), m_th->num(), 0.0);
   Field2D ameridr(m_r->num(), m_th->num(), 0.0), ameridth(m_r->num(), m_th->num(), 0.0);
   Field2D bmeridr(m_r->num(), m_th->num(), 0.0), bmeridth(m_r->num(), m_th->num(), 0.0);

   // Setup derivative arrays
   Field2D ader1(m_r->num(), m_th->num(), 0.0);
   Field2D bder1(m_r->num(), m_th->num(), 0.0);
   Field2D ader2(m_r->num(), m_th->num(), 0.0);
   Field2D bder2(m_r->num(), m_th->num(), 0.0);

   // Get references to field so that calling is easier
   Field2D& an = *m_an;
   Field2D& bn = *m_bn;
   Field1D& eta = *m_eta;
   Field2D& alpha = *m_alpha;
   Field2D& blalpha = *m_bl_alpha;
   Field2D& omega = *m_diffrot;
   Field2D& ur = *m_merid_ur;
   Field2D& uth = *m_merid_uth;
   CVariable& r = *m_r;
   CVariable& th = *m_th;
   CVariable& sin_th = *m_sin_th;
   CVariable& cos_th = *m_cos_th;
   const double dr = m_r->delta();
   const double dth = m_th->delta();

   // Get the parameters we need
   CConfig& config_for_params = m_restart ? m_old_config : m_config;
   const double dt = config_for_params.getParamByName("dt");
   const int    nt = config_for_params.getParamByName("nt");
   const double Ra = config_for_params.getParamByName("calpha");
   const double Rs = config_for_params.getParamByName("cs");
   const double Rw = config_for_params.getParamByName("comega");
   const double Ru = config_for_params.getParamByName("ru");

   const int    logInterval = m_config.getParamByName("logint");

   // Calculate estimated stability criterion here
   double dt_diff = min(r.delta()*r.delta(), th.delta()*th.delta()) / (*eta.maxval());
   double dt_adv = Ru > 0 ? min(r.delta(), th.delta()) / (max(*ur.maxval(), *uth.maxval())) : dt_diff;
   const double scaling_safety_factor = 0.1;
   double est_dt = scaling_safety_factor*min(dt_diff, dt_adv);

   // Start the time loop
   m_logger.Log("Commencing run for " + std::to_string(nt) + " timesteps");
   config_for_params.printParams();
   // Print warning about dt
   if (dt > est_dt)
   {
      m_logger.Log("WARNING: The selected timestep (" + std::to_string(dt) + ") is above the safe maximum (" + std::to_string(est_dt) + ") for stability using these parameters!");
   }
   else
   {
      m_logger.Log("INFO: The selected timestep (" + std::to_string(dt) + ") is below the safe maximum (" + std::to_string(est_dt) + ") for stability using these parameters!");
   }
   m_logger.Log("Press ENTER to begin...");
   std::cin.get();
   // Before we start we create directories etc and write out starting profiles
   // Setup the sim io class - creates output directory and copies 
   // parameter file into output directory
   if (m_restart)
   {
      m_simio.Init(m_old_config, m_config, m_logger);
   }
   else
   {
      m_simio.Init(m_config, m_sConfigFilePath, m_logger);
   }
   // Output static profiles
   Field1D deta(r.num());
   for (int i = 0; i < r.num(); ++i)
   {
      deta(i) = DerivUtils::fd_d1_o2(*m_eta, *m_r, i);
   }
   // calculate shears for plotting only
   Field2D rad_shear(r.num(), th.num(), 0.0);
   Field2D lat_shear(r.num(), th.num(), 0.0);
   for (int i = 0; i < r.num(); ++i)
   {
      for (int j = 0; j < th.num(); ++j)
      {
         rad_shear(i, j) = DerivUtils::fd_d1_o2(*m_diffrot, *m_r, *m_th, 0, i, j);
         lat_shear(i, j) = DerivUtils::fd_d1_o2(*m_diffrot, *m_r, *m_th, 1, i, j);
      }
   }
   m_simio.WriteDiffusivityProfiles(r, eta, deta);
   m_simio.WriteRotationProfiles(r, th, omega, rad_shear, lat_shear);
   m_simio.WriteCirculationProfiles(r, th, ur, uth);
   m_simio.WriteSourceProfiles(r, th, alpha, blalpha);
   // Output time information
   m_simio.WriteTimeParameters(m_config);
   m_logger.Log("Simulation Running...");
   timer.start();
   for (auto t = 1; t < nt+1; ++t)
   {
      // Calculate product quantities
      Field2D ar(an), asinth(an);
      Field2D br(bn), bsinth(an);
      Field2D brur(bn), buth(bn);
      Field2D::MultiplyByVariable(ar, r, 0);
      Field2D::MultiplyByVariable(asinth, sin_th, 1);
      Field2D::MultiplyByVariable(br, r, 0);
      Field2D::MultiplyByVariable(bsinth, sin_th, 1);
      brur = br * ur;
      buth = bn * uth;

      // Calculate all derivative terms on internal points only
      for (auto i = 1; i < m_r->num() - 1; ++i)
      {
         for (auto j = 1; j < m_th->num() - 1; ++j)
         {
            // Calculate the values of certain derivatives at (i,j)
            const double domegadr = DerivUtils::fd_d1_o2(omega, r, th, 0, i, j);
            const double domegadth = DerivUtils::fd_d1_o2(omega, r, th, 1, i, j);
            const double detadr = DerivUtils::fd_d1_o2(eta, r, i);
            const double durdr = DerivUtils::fd_d1_o2(ur, r, th, 0, i, j);
            const double duthdth = DerivUtils::fd_d1_o2(uth, r, th, 1, i, j);

            const double d2b_dr2 = DerivUtils::fd_d2_o2(bn, r, th, 0, i, j);
            const double db_dr = DerivUtils::fd_d1_o2(bn, r, th, 0, i, j);
            const double d2b_dth2 = DerivUtils::fd_d2_o2(bn, r, th, 1, i, j);
            const double db_dth = DerivUtils::fd_d1_o2(bn, r, th, 1, i, j);
            const double d2a_dr2 = DerivUtils::fd_d2_o2(an, r, th, 0, i, j);
            const double da_dr = DerivUtils::fd_d1_o2(an, r, th, 0, i, j);
            const double d2a_dth2 = DerivUtils::fd_d2_o2(an, r, th, 1, i, j);
            const double da_dth = DerivUtils::fd_d1_o2(an, r, th, 1, i, j);

            const double dbrur_dr = DerivUtils::fd_d1_o2(brur, r, th, 0, i, j);
            const double dbuth_dth = DerivUtils::fd_d1_o2(buth, r, th, 1, i, j);
            const double dar_dr = DerivUtils::fd_d1_o2(ar, r, th, 0, i, j);;
            const double dasinth_dth = DerivUtils::fd_d1_o2(asinth, r, th, 1, i, j);;

            /***************************************************/
            /* Toroidal Forcing Terms                          */
            /***************************************************/
            /* Latitudinal Shear Term                          */
            /***************************************************/
            // - D sin(th) / r d(Ar)/dr domega /dth
            blshear(i, j) = -1.0 * domegadth * dar_dr *(sin(th(j)) / r(i));
            /***************************************************/
            /* Radial Shear Term                               */
            /***************************************************/
            // Rw d(Asin(th))/dth domega/dr
            brshear(i, j) = domegadr * dasinth_dth;
            /***************************************************/
            /* Radial Diffusion Term                           */
            /***************************************************/
            // TODO: THIS SEEMS TO BE A TERM THAT IS UNSTABLE BEFORE THE LATITUDINAL TERM - INVESTIGATE (STAGGERED GRID?)
            // 1/r d/dr(eta d(br)/dr) = eta d2B/dr2 + 2eta/r dB/dr + B/r d(eta)/dr + d(eta)/dr dB/dr
            brdiff(i, j) = eta(i) * d2b_dr2 +
                           (2.0*eta(i) / r(i)) * db_dr +
                           (bn(i, j) / r(i)) * detadr +
                           detadr * db_dr;
            /***************************************************/
            /* Latitudinal Diffusion Term                      */
            /***************************************************/
            // eta/r2 d/dth ( 1/sin(th) d/dth(Bsin(th))) = eta/r2 d2B/dth2 + eta/r2 cos(th)/sin(th) dB/dth - eta B/sin2(th) r2
            bldiff(i, j) = (eta(i) / r(i)*r(i)) * d2b_dth2 +
                           (eta(i)*cos(th(j)) / r(i)*r(i)*sin(th(j))) * db_dth +
                           eta(i)*bn(i,j) / r(i)*r(i)*sin(th(j))*sin(th(j));
            /***************************************************/
            /* Radial Advection Term                           */
            /***************************************************/
            // -1/r d/dr (Brur) = -ur/r d(Br)/dr - B dur/dr = -ur/r ( B + r dB/dr ) - B dur/dr
            bmeridr(i, j) = (-1.0/r(i)) * dbrur_dr;
            /***************************************************/
            /* Latitudinal Advection Term                      */
            /***************************************************/
            // -1/r d/dth (Buth) = -uth/r dB/dth - B/r duth/dth
            bmeridth(i, j) = (-1.0 / r(i)) * dbuth_dth;
            /***************************************************/
            /* Poloidal Forcing Terms                          */
            /***************************************************/
            /* Radial Diffusion Term                           */
            /***************************************************/
            // TODO: THIS SEEMS TO BE A TERM THAT IS UNSTABLE BEFORE THE LATITUDINAL TERM - INVESTIGATE (STAGGERED GRID?)
            // eta / r^2 d/dr (r^2 dA/dr) = eta d2A/dr2 + 2 eta / r dA/dr
            ardiff(i, j) = eta(i) * d2a_dr2 +
                           (2.0*eta(i) / r(i)) * da_dr;
            /***************************************************/
            /* Latitudinal Diffusion Term                      */
            /***************************************************/
            // eta / r^2 sin(th) d\dth ( sin(th) dA/dth ) - eta A / r^2 sin^2(th) = eta / r^2 d2A/dth2 +  eta cos(th) / r^2 sin(th) dA/dth - eta A / r^2 sin^2(th)
            aldiff(i, j) =  (eta(i) / r(i)*r(i)) * d2a_dth2 +
                            (eta(i)*cos(th(j)) / r(i)*r(i)*sin(th(j))) * da_dth +
                            eta(i)*an(i, j) / r(i)*r(i)*sin(th(j))*sin(th(j));
            /***************************************************/
            /* Classical Alpha Effect Term                     */
            /***************************************************/
            double bquench_class = 1.0;
            alpha_class(i, j) = (alpha(i, j)*bn(i, j)) / (1.0 + ((bn(i, j)*bn(i, j)) / (bquench_class*bquench_class)));
            /***************************************************/
            /* Babcock Leighton Type Alpha Effect Term         */
            /***************************************************/
            int poloidal_src_idx = int((0.7 - 0.6) / dr);
            double bquench_bl = 1.0;
            alpha_bl(i, j) = (blalpha(i, j)*bn(poloidal_src_idx, j)) / (1.0 + ((bn(poloidal_src_idx, j)*bn(poloidal_src_idx, j)) / (bquench_bl*bquench_bl)));
            /***************************************************/
            /* Radial Advection Term                           */
            /***************************************************/
            // -ur/r d/dr(Ar) = -ur/r (A + r dA/dr)
            ameridr(i, j) = (-1.0*ur(i, j) / r(i))* dar_dr;
            /***************************************************/
            /* Latitudinal Advection Term                      */
            /***************************************************/
            // -uth / rsin(th) d/dth(Asin(th)) = -uth / rsin(th) (Acos(th) + sin(th) dA/dth)
            ameridth(i, j) = (-1.0*uth(i, j) / (r(i)*sin(th(j)))) * dasinth_dth;
         }
      }


      // Add them all together
      bder1 = Rw*brshear + Rw*blshear + brdiff + bldiff + Ru*bmeridr + Ru*bmeridth;
      ader1 = Ra*alpha_class + Rs*alpha_bl + ardiff + aldiff + Ru*ameridr + Ru*ameridth;

      // Do euler for first step
      if (t == 1)
      {
         bn = bn + dt*bder1;
         an = an + dt*ader1;
      }
      // Adam-Bashforth for the rest
      else
      {
         bn = bn + (0.5*dt)*(3.0*bder1 - bder2);
         an = an + (0.5*dt)*(3.0*ader1 - ader2);
      }

      // Swap the derivatives
      bder2 = bder1;
      ader2 = ader1;

      // Apply boundary conditions - only need apply outer radial condition!
      BoundaryConditions upperRadialBc = parseBoundaryConditionConfig(m_config.getParamByName("upperbc"));
      switch (upperRadialBc)
      {
         case BoundaryConditions::Radial:
         {
            // Only for A on the other radial boundary
            // d(Ar)/dr = 0 at the boundary
            // A(nr)r(nr) - A(nr-1)r(nr-1) / 2.0*dr = 0
            int nr = r.dim() - 1;
            int nrm1 = nr - 1;
            for (auto j = 1; j < m_th->num() - 1; ++j)
            {
               an(nr, j) = an(nrm1, j) * (r(nrm1) / r(nr));
            }
            break;
         }
         case BoundaryConditions::Potential:
         {
            int nr = r.dim() - 1;
            int nth = th.dim() - 1;
            // Initialise boundary values
            std::vector<double> boundary_values(th.num(), 0.0);
            for (auto j = 1; j < m_th->num() - 1; ++j)
            {
               boundary_values[j] = 0.0;
            }

            // Need to make sure that dA/dr = 0 at the boundary and matches a potential field
            // dA/dr = 3A(nr) - 4A(nr-1) + A(nr-2) / 2dr = - SUM_l( (l+1) a_l r(nr)^-(l+2) P_l^1(cos(th)) ) 
            // a_n = (2n+1 / 2n(n+1)) r(nr)^n+1 int_0^pi A(nr, th) P_n^1 (cos(th)) sin(th) dth

            // Calculate radial parts of the boundary expansion
            std::vector<double> radial_potential_expansion(ldegree, 0.0);
            for (int degree = 0; degree < ldegree; ++degree)
            {
               radial_potential_expansion[degree] = 1.0;
               for (int j = 0; j < degree; ++j)
               {
                  radial_potential_expansion[degree] = radial_potential_expansion[degree] * (r(nr-1) / r(nr));
               }
            }
            
            // Do integration to find the constants - use an extended simpsons rule
            std::vector<double> consts(ldegree, 0.0);
            for (int degree = 0; degree < ldegree; ++degree)
            {
               consts[degree] = 0.0;
               for (int j = 1; j < nth-1; ++j)
               {
                  consts[degree] = consts[degree] + an(nr, j)*(4.0 / 3.0)*sin(th(j))*legendre[degree][j] + an(nr, j+1)*(2.0 / 3.0)*sin(th(j+1))*legendre[degree][j+1];
               }
            }

            // Do the sum
            for (int j = 1; j < nth-1; ++j)
            {
               for (int degree = 0; degree < ldegree; ++degree)
               {
                  boundary_values[j] = boundary_values[j] + consts[degree]*legendre[degree][j]*radial_potential_expansion[degree];
               }
            }

            for (int j = 0; j < th.num(); ++j)
            {  
               an(nr, j) = boundary_values[j];
            }
            break;
         }
      }
      calculate_components_from_axisymmetric_fields();

      if (m_simio.ShouldWriteFields(t))
      {
         m_simio.WriteMagneticField(*m_r, *m_th, *m_an, *m_bn);
      }

      if (m_simio.ShouldWriteButterflies(t))
      {
         m_simio.AppendButterfly(bn, int((0.7 - 0.6) / dr), "butt.txt");
         m_simio.AppendButterfly(*m_br, r.num()-1, "radbutt.txt");
      }

      double energy = calculate_energy();
      if (m_simio.ShouldWriteQuantity(t))
      {
         m_simio.AppendData(energy, "energy.txt");
         //m_simio.AppendData(calculate_parity(), "parity.txt");
      }

      if (t % logInterval == 0)
      {
         m_logger.LogTimestepAndEnergy(t, dt, energy);
      }
   }

   timer.finish();
   m_logger.Log("Run Complete in " + timer.get_elapsed_time_string());
}

void CSimulation::calculate_components_from_axisymmetric_fields()
{
   // Start off with phi component - easy
   m_bphi = std::make_unique<Field2D>(*m_bn);
   // Calculate two parts we need
   // Create the new mesh
   Field2D dar(m_an->dim(0), m_an->dim(1));
   Field2D dath(m_an->dim(0), m_an->dim(1));
   // Need to calculate derivatives from a now
   DerivUtils::fd_deriv_1st_order_2nd(*m_an, *m_r, 0, dar);
   DerivUtils::fd_deriv_1st_order_2nd(*m_an, *m_th, 1, dath);
   // Useful shizz
   Field2D& an = *m_an;
   Field2D& bn = *m_bn;
   Field2D& br = *m_br;
   Field2D& bth = *m_bth;
   CVariable& r = *m_r;
   CVariable& th = *m_th;
   const double dr = m_r->delta();
   const double dth = m_th->delta();

   // Calculate br = 1 / rsin(th) * d(asin(th))/dth = 1/r da/dth + acos(th)/rsin(th)
   // Calcuate bth = - 1 / r * d(ar)/dr = -da/dr - a/r
   for (int i = 0; i < m_an->dim(0); ++i)
   {
      for (int j = 0; j < m_an->dim(1); ++j)
      {
         br(i, j) = dath(i, j)*(1.0 / r(i)) + an(i, j)*(cos(th(j)) / (r(i)*sin(th(j))));
         bth(i, j) = dar(i, j)*(-1.0) - (an(i, j) / r(i));
      }
   }
}

double CSimulation::calculate_energy()
{
   double em = 0;
   // Get references to field so that calling is easier
   Field2D& a = *m_an;
   Field2D& b = *m_bn;
   CVariable& r = *m_r;
   CVariable& th = *m_th;
   // Temp values
   double temp = 0.0;
   double temp2 = 0.0;
   double temp3 = 0.0;
   // DO IT!
   for (int i = 1; i < m_r->num()-1; ++i)
   {
      for (int j = 1; j < m_th->num()-1; ++j)
      {
         temp = ((b(i+1, j+1) + b(i-1, j+1) + b(i-1, j-1) + b(i+1, j-1)) / 4.0);
         temp2 = (((sin(th(j+1))*a(i, j+1)) - (sin(th(j-1))*a(i, j-1))) / (2.0*th.delta()*r(i)*sin(th(j))));
         temp3 = (((r(i+1)*(a(i+1, j+1) + a(i+1, j-1))) - (r(i-1)*(a(i-1, j+1) + a(i-1, j-1)))) / (2.0*(r(i+1) - r(i-1))));
         em += (2.0*th.delta()*((temp*temp) + (temp2*temp2))*r(i)*r(i)*sin(th(j))*(r(i+1) - r(i-1))) + (2.0*th.delta()*temp3*temp3*sin(th(j))*(r(i+1) - r(i-1)));
      }
   }
   return em;
}

double CSimulation::calculate_parity()
{
   // Split A and B into Symmetric and AntiSymmetric parts
   // T_ij = S_ij + A_ij
   // S_ij = 0.5 ( T_ij + T_ji )
   // A_ij = 0.5 ( T_ij - T_ji )
   return 0.0;
}