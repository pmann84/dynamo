#include "BoundaryConditions.h"
#include "../DynamoConfig/ConfigExceptions.h"

BoundaryConditions parseBoundaryConditionConfig(const std::string condition)
{
   if (condition == "radial")
   {
      return BoundaryConditions::Radial;
   }
   if (condition == "potential")
   {
      return BoundaryConditions::Potential;
   }
   throw XInvalidConfigParameter(condition);
}