#include "ConfigValidator.h"
#include "BoundaryConditions.h"

ConfigValidator::ConfigValidator()
{
}

ConfigValidator::~ConfigValidator()
{
}

bool ConfigValidator::validate(const CConfig& config)
{
   try
   {
      parseBoundaryConditionConfig(config.getParamByName("upperbc"));
   }
   catch(...)
   {
      return false;
   }
   return true;
}
