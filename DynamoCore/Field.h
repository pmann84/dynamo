﻿#ifndef __FIELD_H__
#define __FIELD_H__

#include <functional>

#include "Mesh.h"
#include "variable.h"

class Field1D : public CMesh1D
{
public:
   Field1D(int n);
   ~Field1D();

   void initialise(double(*initFn)(double), CVariable &v);
private:

};

class Field2D : public CMesh2D
{
public:
   Field2D(int n1, int n2);
   Field2D(int n1, int n2, double val);
   Field2D(Field2D& f);
   ~Field2D();

   void initialise(double val);
   void initialise(std::function<double(double, double)> init_fn, const CVariable &v1, const CVariable &v2);

   static void MultiplyByVariable(Field2D& f, const CVariable& v, int dimIdx);

   // Operator overloads
   // + to add 2 Mesh2D objects together
   friend Field2D operator+(const Field2D &m1, const Field2D &m2);
   // + to add a Mesh2D and double together
   friend Field2D operator+(double val, const Field2D &m2);
   // - to subtract 2 Mesh2D objects from each other
   friend Field2D operator-(const Field2D &m1, const Field2D &m2);
   // * to multiply 2 Mesh2D objects together (component wise multiplication)
   friend Field2D operator*(const Field2D &m1, const Field2D &m2);
   // * to multiply a Mesh2D and a double together (component wise multiplication)
   friend Field2D operator*(double val, const Field2D &m1);
   // / to divide 2 Mesh2D objects together (component wise division)
   friend Field2D operator/(const Field2D &m1, const Field2D &m2);
   // Overload assignment operator
   void operator=(const Field2D &f);

private:

};

#endif