﻿#include "DerivUtils.h"

double DerivUtils::fd_d1_o2(const CMesh2D& f, const CVariable& x, const CVariable& y, const int dim, const int i, const int j)
{
   if (dim == 0)
   {
      if (i == x.num()-1)
      {
         return (3.0*f(i, j) - 4.0*f(i-1, j) + f(i-2, j)) / (2.0*x.delta());
      }
      else if (i == 0)
      {
         return (-3.0*f(i, j) + 4.0*f(i+1, j) - f(i+2, j)) / (2.0*x.delta());
      }
      return (f(i+1, j) - f(i-1, j)) / (2.0*x.delta());
   }
   else
   {
      if (j == y.num() - 1)
      {
         return (3.0*f(i, j) - 4.0*f(i, j-1) + f(i, j-2)) / (2.0*y.delta());
      }
      else if (j == 0)
      {
         return (-3.0*f(i, j) + 4.0*f(i, j+1) - f(i, j+2)) / (2.0*y.delta());
      }
      return (f(i, j+1) - f(i, j-1)) / (2.0*y.delta());
   }
}


double DerivUtils::fd_d1_o2(const CMesh1D& f, const CVariable& x, const int i)
{
   if (i == x.num() - 1)
   {
      return (3.0*f(i) - 4.0*f(i-1) + f(i-2)) / (2.0*x.delta());
   }
   else if (i == 0)
   {
      return (-3.0*f(i) + 4.0*f(i+1) - f(i+2)) / (2.0*x.delta());
   }
   return (f(i+1) - f(i-1)) / (2.0*x.delta());
}

double DerivUtils::fd_d2_o2(const CMesh2D& f, const CVariable& x, const CVariable& y, const int dim, const int i, const int j)
{
   if (dim == 0)
   {
      // Second order second derivative
      if (i == 0)
      {
         return (2.0*f(i, j) - 5.0*f(i+1, j) + 4.0*f(i+2, j) - f(i+3, j)) / (x.delta()*x.delta());
      }
      else if (i == x.dim() - 1)
      {
         return (2.0*f(i, j) - 5.0*f(i-1, j) + 4.0*f(i-2, j) - f(i-3, j)) / (x.delta()*x.delta());
      }
      return (f(i-1, j) - 2.0*f(i, j) + f(i+1, j)) / (x.delta()*x.delta());
   }
   else
   {
      // Second order second derivative
      if (j == 0)
      {
         return (2.0*f(i, j) - 5.0*f(i, j+1) + 4.0*f(i, j+2) - f(i, j+3)) / (y.delta()*y.delta());
      }
      else if (j == y.dim() - 1)
      {
         return (2.0*f(i, j) - 5.0*f(i, j-1) + 4.0*f(i, j-2) - f(i, j-3)) / (y.delta()*y.delta());
      }
      return (f(i, j-1) - 2.0*f(i, j) + f(i, j+1)) / (y.delta()*y.delta());
   }
}

double DerivUtils::fd_d2_o2(const CMesh1D& f, const CVariable& x, const int i)
{
   // Second order second derivative
   if (i == 0)
   {
      return (2.0*f(i) - 5.0*f(i+1) + 4.0*f(i+2) - f(i+3)) / (x.delta()*x.delta());
   }
   else if (i == x.dim() - 1)
   {
      return (2.0*f(i) - 5.0*f(i-1) + 4.0*f(i-2) - f(i-3)) / (x.delta()*x.delta());
   }
   return (f(i-1) - 2.0*f(i) + f(i+1)) / (x.delta()*x.delta());
}

void DerivUtils::fd_deriv_1st_order_2nd(const CMesh2D& f, const CVariable& x, const int j, CMesh2D &df)
{
   auto dinv = 1.0 / x.delta();

   auto deriv_coeff = 0.5; // 1/2

   auto onesided_coeff_0 = -1.5;
   auto onesided_coeff_1 = 2.0;
   auto onesided_coeff_2 = -0.5;

   if (j == 0)
   {
      for (auto ii = 0; ii < f.dim(0); ++ii)
      {
         for (auto jj = 0; jj < f.dim(1); ++jj)
         {
            if (ii == 0)
            {
               auto deriv_val = dinv*(onesided_coeff_0*f(ii, jj) +
                  onesided_coeff_1*f(ii + 1, jj) +
                  onesided_coeff_2*f(ii + 2, jj));
               df(ii, jj) = deriv_val;
            }
            else if (ii == f.dim(0) - 1)
            {
               auto deriv_val = dinv*(-onesided_coeff_0*f(ii, jj)
                  - onesided_coeff_1*f(ii - 1, jj)
                  - onesided_coeff_2*f(ii - 2, jj));
               df(ii, jj) = deriv_val;
            }
            else
            {
               auto deriv_val = dinv*deriv_coeff*(f(ii + 1, jj) -
                  f(ii - 1, jj));
               df(ii, jj) = deriv_val;
            }
         }
      }
   }
   else if (j == 1)
   {
      for (auto ii = 0; ii < f.dim(0); ++ii)
      {
         for (auto jj = 0; jj < f.dim(1); ++jj)
         {
            if (jj == 0)
            {
               auto deriv_val = dinv*(onesided_coeff_0*f(ii, jj) +
                  onesided_coeff_1*f(ii, jj + 1) +
                  onesided_coeff_2*f(ii, jj + 2));
               df(ii, jj) = deriv_val;
            }
            else if (jj == f.dim(1) - 1)
            {
               auto deriv_val = dinv*(-onesided_coeff_0*f(ii, jj)
                  - onesided_coeff_1*f(ii, jj - 1)
                  - onesided_coeff_2*f(ii, jj - 2));
               df(ii, jj) = deriv_val;
            }
            else
            {
               auto deriv_val = dinv*deriv_coeff*(f(ii, jj + 1) -
                  f(ii, jj - 1));
               df(ii, jj) = deriv_val;
            }
         }
      }
   }
   else
   {
      throw std::exception("This method only calculates derivatives in 2 dimensions");
   }
}

void DerivUtils::fd_deriv_2nd_order_2nd(const CMesh2D& f, const CVariable& x, const int j, CMesh2D &df)
{
   auto dinv = 1.0 / (x.delta()*x.delta());

   auto deriv_coeff_0 = -2.0;
   auto deriv_coeff_1 = 1.0;

   auto onesided_coeff_0 = 2.0;
   auto onesided_coeff_1 = -5.0;
   auto onesided_coeff_2 = 4.0;
   auto onesided_coeff_3 = -1.0;

   if (j == 0)
   {
      for (auto ii = 0; ii < f.dim(0); ++ii)
      {
         for (auto jj = 0; jj < f.dim(1); ++jj)
         {
            if (ii == 0)
            {
               auto deriv_val = dinv*(onesided_coeff_0*f(ii, jj) +
                  onesided_coeff_1*f(ii + 1, jj) +
                  onesided_coeff_2*f(ii + 2, jj) +
                  onesided_coeff_3*f(ii + 3, jj));
               df(ii, jj)  = deriv_val;
            }
            else if (ii == f.dim(0) - 1)
            {
               auto deriv_val = dinv*(-onesided_coeff_0*f(ii, jj)
                  - onesided_coeff_1*f(ii - 1, jj)
                  - onesided_coeff_2*f(ii - 2, jj)
                  - onesided_coeff_3*f(ii - 3, jj));
               df(ii, jj) = deriv_val;
            }
            else
            {
               auto deriv_val = dinv*(deriv_coeff_1*f(ii + 1, jj) +
                  deriv_coeff_0*f(ii, jj) +
                  deriv_coeff_1*f(ii - 1, jj));
               df(ii, jj) = deriv_val;
            }
         }
      }
   }
   else if (j == 1)
   {
      for (auto ii = 0; ii < f.dim(0); ++ii)
      {
         for (auto jj = 0; jj < f.dim(1); ++jj)
         {
            if (jj == 0)
            {
               auto deriv_val = dinv*(onesided_coeff_0*f(ii, jj) +
                  onesided_coeff_1*f(ii, jj + 1) +
                  onesided_coeff_2*f(ii, jj + 2) +
                  onesided_coeff_3*f(ii, jj + 3));
               df(ii, jj) = deriv_val;
            }
            else if (jj == f.dim(1) - 1)
            {
               auto deriv_val = dinv*(-onesided_coeff_0*f(ii, jj)
                  - onesided_coeff_1*f(ii, jj - 1)
                  - onesided_coeff_2*f(ii, jj - 2)
                  - onesided_coeff_3*f(ii, jj - 3));
               df(ii, jj) = deriv_val;
            }
            else
            {
               auto deriv_val = dinv*(deriv_coeff_1*f(ii, jj + 1) +
                  deriv_coeff_0*f(ii, jj) +
                  deriv_coeff_1*f(ii, jj - 1));
               df(ii, jj) = deriv_val;
            }
         }
      }
   }
   else
   {
      throw std::exception("This method only calculates derivatives in 2 dimensions");
   }
}

//void fd_deriv_1st_order_4th(Mesh2D f, CVariable x, int j, Mesh2D &df)
//{
//	auto dinv = 1.0 / x.delta();
//
//	auto deriv_coeff_1 = 0.66666; // 2/3
//	auto deriv_coeff_2 = -0.08333; // -1/12
//
//	if (j == 0)
//	{
//		for (auto ii = 1; ii < f.dim(0)-1; ++ii)
//		{
//			for (auto jj = 0; jj < f.dim(1); ++jj)
//			{
//				auto deriv_val = 0.0;
//				if (ii == 1)
//				{
//					deriv_val = -3.0*f.get(ii - 1, jj) - 10.0*f.get(ii, jj) + 18.0*f.get(ii + 1, jj) - 6.0*f.get(ii + 2, jj) + 1.0*f.get(ii + 3, jj);
//				}
//				else if (ii == f.dim(0)-2)
//				{
//					deriv_val = 3.0*f.get(ii + 1, jj) + 10.0*f.get(ii, jj) - 18.0*f.get(ii - 1, jj) + 6.0*f.get(ii - 2, jj) - 1.0*f.get(ii - 3, jj);
//				}
//				else
//				{
//					deriv_val = (dinv*
//								(deriv_coeff_1*(f.get(ii + 1, jj) - f.get(ii - 1, jj)) + 
//								 deriv_coeff_2*(f.get(ii + 2, jj) - f.get(ii - 2, jj))));
//				}
//				df.set(ii, jj, deriv_val);
//			}
//		}
//	}
//	else if (j == 1)
//	{
//		for (auto ii = 0; ii < f.dim(0); ++ii)
//		{
//			for (auto jj = 1; jj < f.dim(1)-1; ++jj)
//			{
//				auto deriv_val = 0.0;
//				if (jj == 1)
//				{
//					deriv_val = -3.0*f.get(ii, jj - 1) - 10.0*f.get(ii, jj) + 18.0*f.get(ii, jj + 1) - 6.0*f.get(ii, jj + 2) + 1.0*f.get(ii, jj + 3);
//				}
//				else if (jj == f.dim(1) - 2)
//				{
//					deriv_val = 3.0*f.get(ii, jj + 1) + 10.0*f.get(ii, jj) - 18.0*f.get(ii, jj - 1) + 6.0*f.get(ii, jj - 2) - 1.0*f.get(ii, jj - 3);
//				}
//				else
//				{
//					deriv_val = (dinv*
//								(deriv_coeff_1*(f.get(ii, jj + 1) - f.get(ii, jj - 1)) +
//								 deriv_coeff_2*(f.get(ii, jj + 2) - f.get(ii, jj - 2))));
//				}
//				df.set(ii, jj, deriv_val);
//			}
//		}
//	}
//	else
//	{
//		throw std::exception("This method only calculates derivatives in 2 dimensions");
//	}
//}
//
//void fd_deriv_1st_order_6th(Mesh2D f, CVariable x, int j, Mesh2D &df)
//{
//	auto dinv = 1.0 / x.delta();
//
//	auto deriv_coeff_1 = 0.75; // 3/4
//	auto deriv_coeff_2 = -0.15; // -3/20
//	auto deriv_coeff_3 = 0.016666; // 1/60 
//
//	if (j == 0)
//	{
//		for (auto ii = 0; ii < f.dim(0); ++ii)
//		{
//			for (auto jj = 0; jj < f.dim(1); ++jj)
//			{
//				auto deriv_val = (dinv*
//								 (deriv_coeff_1*(f.get(ii + 1, jj) - f.get(ii - 1, jj)) +
//								  deriv_coeff_2*(f.get(ii + 2, jj) - f.get(ii - 2, jj)) +
//								  deriv_coeff_3*(f.get(ii + 3, jj) - f.get(ii - 3, jj))));
//				df.set(ii, jj, deriv_val);
//			}
//		}
//	}
//	else if (j == 1)
//	{
//		for (auto ii = 0; ii < f.dim(0); ++ii)
//		{
//			for (auto jj = 0; jj < f.dim(1); ++jj)
//			{
//				auto deriv_val = (dinv*
//								 (deriv_coeff_1*(f.get(ii, jj + 1) - f.get(ii, jj - 1)) +
//								  deriv_coeff_2*(f.get(ii, jj + 2) - f.get(ii, jj - 2)) +
//								  deriv_coeff_3*(f.get(ii, jj + 3) - f.get(ii, jj - 3))));
//				df.set(ii, jj, deriv_val);
//			}
//		}
//	}
//	else
//	{
//		throw std::exception("This method only calculates derivatives in 2 dimensions");
//	}
//}
//
//void fd_deriv_1st_order_8th(Mesh2D f, CVariable x, int j, Mesh2D &df)
//{
//	auto dinv = 1.0 / x.delta();
//
//	auto deriv_coeff_1 = 0.8; // 4/5
//	auto deriv_coeff_2 = -0.2; // -1/5
//	auto deriv_coeff_3 = 0.0380952; // 4/105
//	auto deriv_coeff_4 = -0.00357142857; // -1/280
//
//	if (j == 0)
//	{
//		for (auto ii = 0; ii < f.dim(0); ++ii)
//		{
//			for (auto jj = 0; jj < f.dim(1); ++jj)
//			{
//				auto deriv_val = (dinv*
//								 (deriv_coeff_1*(f.get(ii + 1, jj) - f.get(ii - 1, jj)) +
//								  deriv_coeff_2*(f.get(ii + 2, jj) - f.get(ii - 2, jj)) +
//								  deriv_coeff_3*(f.get(ii + 3, jj) - f.get(ii - 3, jj)) +
//								  deriv_coeff_4*(f.get(ii + 4, jj) - f.get(ii - 4, jj))));
//				df.set(ii, jj, deriv_val);
//			}
//		}
//	}
//	else if (j == 1)
//	{
//		for (auto ii = 0; ii < f.dim(0); ++ii)
//		{
//			for (auto jj = 0; jj < f.dim(1); ++jj)
//			{
//				auto deriv_val = (dinv*
//								 (deriv_coeff_1*(f.get(ii + 1, jj) - f.get(ii - 1, jj)) +
//								  deriv_coeff_2*(f.get(ii + 2, jj) - f.get(ii - 2, jj)) +
//								  deriv_coeff_3*(f.get(ii + 3, jj) - f.get(ii - 3, jj)) +
//								  deriv_coeff_4*(f.get(ii + 4, jj) - f.get(ii - 4, jj))));
//				df.set(ii, jj, deriv_val);
//			}
//		}
//	}
//	else
//	{
//		throw std::exception("This method only calculates derivatives in 2 dimensions");
//	}
//}

// Following methods calculate df / dx_j for different orders of accuracy
//void fd_deriv_1st_order_4th_upwind(Mesh2D f, CVariable x, Mesh2D u, int j, Mesh2D &df)
//{
//	auto dinv = 1.0 / x.delta();
//	auto coeffm1 = 3.0 / 12.0;
//	auto coeff0 = 10.0 / 12.0;
//	auto coeff1 = -18.0 / 12.0;
//	auto coeff2 = 6.0 / 12.0;
//	auto coeff3 = -1.0 / 12.0;
//
//	if (j == 0)
//	{
//		for (auto ii = 0; ii < f.dim(0); ++ii)
//		{
//			for (auto jj = 0; jj < f.dim(1); ++jj)
//			{
//				if (u.get(ii,jj) >= 0)
//				{
//					if (ii == 0)
//					{
//						
//					}
//					else if (ii == f.dim(0) - 1)
//					{
//						
//					}
//					else
//					{
//						auto deriv_val = dinv*(coeff3*f.get(ii - 3, jj) + coeff2*f.get(ii - 2, jj) + coeff1*f.get(ii - 1, jj) + coeff0*f.get(ii, jj) + coeffm1*f.get(ii + 1, jj));
//						df.set(ii, jj, deriv_val);
//					}
//				}
//				else
//				{
//					if (ii == 0)
//					{
//
//					}
//					else if (ii == f.dim(0) - 1)
//					{
//
//					}
//					else
//					{
//						auto deriv_val = -1.0*dinv*(coeff3*f.get(ii + 3, jj) + coeff2*f.get(ii + 2, jj) + coeff1*f.get(ii + 1, jj) + coeff0*f.get(ii, jj) + coeffm1*f.get(ii - 1, jj));
//						df.set(ii, jj, deriv_val);
//					}
//				}
//			}
//		}
//	}
//	else if (j == 1)
//	{
//		for (auto ii = 0; ii < f.dim(0); ++ii)
//		{
//			for (auto jj = 0; jj < f.dim(1); ++jj)
//			{
//				if (u.get(ii, jj) >= 0)
//				{
//					if (jj == 0)
//					{
//
//					}
//					else if (jj == f.dim(1) - 1)
//					{
//
//					}
//					else
//					{
//						auto deriv_val = dinv*(coeff3*f.get(ii, jj - 3) + coeff2*f.get(ii, jj - 2) + coeff1*f.get(ii, jj - 1) + coeff0*f.get(ii, jj) + coeffm1*f.get(ii, jj + 1));
//						df.set(ii, jj, deriv_val);
//					}
//				}
//				else
//				{
//					if (jj == 0)
//					{
//
//					}
//					else if (jj == f.dim(1) - 1)
//					{
//
//					}
//					else
//					{
//						auto deriv_val = -1.0*dinv*(coeff3*f.get(ii, jj + 3) + coeff2*f.get(ii, jj + 2) + coeff1*f.get(ii, jj + 1) + coeff0*f.get(ii, jj) + coeffm1*f.get(ii, jj - 1));
//						df.set(ii, jj, deriv_val);
//					}
//				}
//			}
//		}
//	}
//	else
//	{
//		throw std::exception("This method only calculates derivatives in 2 dimensions");
//	}
//	
//}