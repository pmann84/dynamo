﻿#include "Mesh.h"
#include "Variable.h"

namespace DerivUtils
{
   // These functions are so named:
   // fd_d{derivative order}_o{error order}{_{extra info (optional)}}
   // e.g. fd_d1_d2() would represent a function that calculates the 1st derivative to 2nd order accuracy

   double fd_d1_o2(const CMesh2D& f, const CVariable& x, const CVariable& y, const int dim, const int i, const int j); // 2D
   double fd_d1_o2(const CMesh1D& f, const CVariable& x, const int i); // 1D

   double fd_d2_o2(const CMesh2D& f, const CVariable& x, const CVariable& y, const int dim, const int i, const int j); // 2D
   double fd_d2_o2(const CMesh1D& f, const CVariable& x, const int i); // 1D

   void fd_deriv_1st_order_2nd(const CMesh2D& f, const CVariable& x, const int j, CMesh2D &df);
   void fd_deriv_2nd_order_2nd(const CMesh2D& f, const CVariable& x, const int j, CMesh2D &df);
}


//void fd_deriv_1st_order_4th_upwind(Mesh2D f, CVariable x, Mesh2D u, int j, Mesh2D &df)
//void fd_deriv_1st_order_4th(Mesh2D f, CVariable x, int j, Mesh2D &df);
//void fd_deriv_1st_order_6th(Mesh2D f, CVariable x, int j, Mesh2D &df);
//void fd_deriv_1st_order_8th(Mesh2D f, CVariable x, int j, Mesh2D &df);


// Write a class as follows:
//class finite_difference
//{
//public:
//	finite_difference(int max_deriv);
//	~finite_difference();
//
//	void calculate_derivative(Mesh1D m, CVariable v, Mesh1D d);
//	void calculate_derivative(Mesh2D mesh, CVariable v, Mesh2D &deriv, int deriv_dim);
//private:
//	int m_max_deriv;
//};
//
//finite_difference::finite_difference(int max_deriv) : m_max_deriv(max_deriv)
//{
//}
//
//finite_difference::~finite_difference()
//{
//}

//TESTING code for finite difference weights
//std::cout << "FINITE DIFFERENCE WEIGHTS Test: " << std::endl;
//std::vector<int> x = {-2, -1, 0, 1, 2};
//std::vector< std::vector<double> > weights;
//weights = calculate_finite_difference_weights(0.0, x, 6);
//std::vector< std::vector<double> >::iterator it1 = weights.begin();
//for (it1; it1 != weights.end(); ++it1)
//{
//	std::vector<double>::iterator it2 = (*it1).begin();
//	for (it2; it2 != (*it1).end(); ++it2)
//	{
//		std::cout << (*it2) << " ";
//	}
//	std::cout << std::endl;
//}

/*The following Matlab code(Fornberg 1998) calculates FD weights on arbitrarily spaced 1 - D node sets

function c = weights(z,x,m)
% Calculates FD weights. The parameters are:
%  z   location where approximations are to be accurate,
%  x   vector with x-coordinates for grid points,
%  m   highest derivative that we want to find weights for
%  c   array size m+1,lentgh(x) containing (as output) in
%      successive rows the weights for derivatives 0,1,...,m.

n=length(x); c=zeros(m+1,n); c1=1; c4=x(1)-z; c(1,1)=1;
for i=2:n
mn=min(i,m+1); c2=1; c5=c4; c4=x(i)-z;
for j=1:i-1
c3=x(i)-x(j);  c2=c2*c3;
if j==i-1
c(2:mn,i)=c1*((1:mn-1)'.*c(1:mn-1,i-1)-c5*c(2:mn,i-1))/c2;
c(1,i)=-c1*c5*c(1,i-1)/c2;
end
c(2:mn,j)=(c4*c(2:mn,j)-(1:mn-1)'.*c(1:mn-1,j))/c3;
c(1,j)=c4*c(1,j)/c3;
end
c1=c2;
end
For example, the statement weights(0, -2:2, 6) returns the output

0         0    1.0000         0         0
0.0833 - 0.6667         0    0.6667 - 0.0833
- 0.0833    1.3333 - 2.5000    1.3333 - 0.0833
- 0.5000    1.0000         0 - 1.0000    0.5000
1.0000 - 4.0000    6.0000 - 4.0000    1.0000
0         0         0         0         0
0         0         0         0         0
This shows the optimal weights to be applied to function values at x = -2, -1, 0, 1, 2 for approximating
the 0th up through the 6th derivative at x = 0.The last two lines are all zero, since there exist no formulas
for the 5th and the 6th derivative that extend over only 5 node points. */

//subroutine populate_weights(z, x, nd, m, c)
//  !
//  !  Input Parameters
//  !    z            -  location where approximations are to be
//  !                    accurate
//  !    x(0:nd)      -  grid point locations, found in x(0:n)
//  !    nd           -  dimension of x- and c-arrays in calling
//  !                    program x(0:nd) and c(0:nd, 0:m), respectively
//  !    m            -  highest derivative for which weights are
//  !                    sought
//  !
//  !  Output Parameter
//  !    c(0:nd,0:m)  -  weights at grid locations x(0:n) for
//  !                    derivatives of order 0:m, found in c(0:nd, 0:m)
//  !
//  !  Reference:
//  !      Generation of Finite Difference Formulas on Arbitrarily
//  !          Spaced Grids, Bengt Fornberg,
//  !          Mathematics of compuation, 51, 184, 1988, 699-706

//  real(dp), intent(in)    :: z
//  integer, intent(in)     :: nd, m
//  real(dp), intent(in)    :: x(0:nd)
//  real(dp), intent(out) :: c(0:nd, 0:m)

//  real(dp) :: c1, c2, c3, c4, c5
//  integer :: i, j, k, mn

//  c1 = 1
//  c4 = x(0) - z
//  c = 0
//  c(0, 0) = 1
//  do i=1, nd
//    mn = min(i, m)
//    c2 = 1
//    c5 = c4
//    c4 = x(i) - z
//    do j=0, i-1
//      c3 = x(i) - x(j)
//      c2 = c2*c3
//      if (j == i-1) then
//        do k = mn, 1, -1
//          c(i, k) = c1*(k*c(i-1, k-1) - c5*c(i-1, k))/c2
//        end do
//        c(i, 0) = -c1*c5*c(i-1, 0)/c2
//      endif
//      do k=mn, 1, -1
//        c(j, k) = (c4*c(j, k) - k*c(j, k-1))/c3
//      end do
//      c(j, 0) = c4*c(j, 0)/c3
//    end do
//    c1 = c2
//  end do
//end subroutine

//std::vector< std::vector<double> > calculate_finite_difference_weights(double x0, std::vector<int> x, int m) //, std::vector< std::vector<double> > &weights)
//{
//	// x0 - location approximation should be accurate at
//	// x - vector of ints that give coordinates for grid points
//	// m - highest derivative to calculate for
//
//	// ALGORITHM
//
//	// Size of array of grid pts
//	int n = x.size();
//	int mn;
//	// Initialise temporary variables
//	double c1(1.0), c2, c3, c4(x[0]-x0), c5;
//	// Initialise some members of array to store weights
//	std::vector< std::vector<double> > weights(n, std::vector<double>(m+1));
//	weights[0][0] = 1.0;
//	return weights;
//}