﻿#ifndef __MESH_H__
#define __MESH_H__

#include <string>

#include "IMesh.h"

class CMesh1D : public IMesh
{
public:
   explicit CMesh1D(int n);
   ~CMesh1D() = default;

   int dim() const;

   // Overload () accessor operator
   double operator()(const int i) const; // Getting
   double& operator()(const int i); // Setting

   // IMesh Interface Implementations
   std::vector<double>::iterator minval() override;
   std::vector<double>::iterator maxval() override;
   std::vector<double>::const_iterator minval() const override;
   std::vector<double>::const_iterator maxval() const override;
   bool validate() override;
   void print_mesh() override;
private:
   int m_n;
   std::vector<double> m_mesh;
};

class CMesh2D : public IMesh
{
public:
   CMesh2D(int n1, int n2);
   ~CMesh2D() = default;

   int dim(int d) const;

   // Utility function
   std::string getMeshStats() const;

   // Operator overloads
   // + to add 2 CMesh2D objects together
   friend CMesh2D operator+(const CMesh2D &m1, const CMesh2D &m2);
   // - to subtract 2 CMesh2D objects from each other
   friend CMesh2D operator-(const CMesh2D &m1, const CMesh2D &m2);
   // * to multiply 2 CMesh2D objects together (component wise multiplication)
   friend CMesh2D operator*(const CMesh2D &m1, const CMesh2D &m2);
   // * to multiply a CMesh2D and a double together (component wise multiplication)
   friend CMesh2D operator*(double val, const CMesh2D &m1);
   // Overload () accessor operator
   double operator()(const int i, const int j) const; // Getting
   double& operator()(const int i, const int j); // Setting
   // Overload assignment operator
   void operator=(const CMesh2D &m);

   // IMesh Interface Implementations
   std::vector<double>::iterator minval() override;
   std::vector<double>::iterator maxval() override;
   std::vector<double>::const_iterator minval() const override;
   std::vector<double>::const_iterator maxval() const override;
   bool validate() override;
   void print_mesh() override;

private:
   int m_n1;
   int m_n2;
   std::vector<double> m_mesh;
};

#endif